"use weex:vue";
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!******************************************************************************************************************!*\
  !*** /Users/caijizhou/Documents/HBuilderProjects/yoki-live/main.js?{"page":"pages%2Fcreate-live%2Fcreate-live"} ***!
  \******************************************************************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var uni_app_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uni-app-style */ 1);\n/* harmony import */ var uni_app_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uni_app_style__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _pages_create_live_create_live_nvue_mpType_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/create-live/create-live.nvue?mpType=page */ 4);\n\n        \n        \n        \n        if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {\n          Promise.prototype.finally = function(callback) {\n            var promise = this.constructor\n            return this.then(function(value) {\n              return promise.resolve(callback()).then(function() {\n                return value\n              })\n            }, function(reason) {\n              return promise.resolve(callback()).then(function() {\n                throw reason\n              })\n            })\n          }\n        }\n        _pages_create_live_create_live_nvue_mpType_page__WEBPACK_IMPORTED_MODULE_1__[\"default\"].mpType = 'page'\n        _pages_create_live_create_live_nvue_mpType_page__WEBPACK_IMPORTED_MODULE_1__[\"default\"].route = 'pages/create-live/create-live'\n        _pages_create_live_create_live_nvue_mpType_page__WEBPACK_IMPORTED_MODULE_1__[\"default\"].el = '#root'\n        new Vue(_pages_create_live_create_live_nvue_mpType_page__WEBPACK_IMPORTED_MODULE_1__[\"default\"])\n        //# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbbnVsbF0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLFFBQThCO0FBQzlCLFFBQTBFO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixhQUFhO0FBQ2I7QUFDQTtBQUNBLGVBQWU7QUFDZixhQUFhO0FBQ2I7QUFDQTtBQUNBLFFBQVEsdUZBQUc7QUFDWCxRQUFRLHVGQUFHO0FBQ1gsUUFBUSx1RkFBRztBQUNYLGdCQUFnQix1RkFBRyIsImZpbGUiOiIwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG4gICAgICAgIFxuICAgICAgICBpbXBvcnQgJ3VuaS1hcHAtc3R5bGUnXG4gICAgICAgIGltcG9ydCBBcHAgZnJvbSAnLi9wYWdlcy9jcmVhdGUtbGl2ZS9jcmVhdGUtbGl2ZS5udnVlP21wVHlwZT1wYWdlJ1xuICAgICAgICBpZiAodHlwZW9mIFByb21pc2UgIT09ICd1bmRlZmluZWQnICYmICFQcm9taXNlLnByb3RvdHlwZS5maW5hbGx5KSB7XG4gICAgICAgICAgUHJvbWlzZS5wcm90b3R5cGUuZmluYWxseSA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICB2YXIgcHJvbWlzZSA9IHRoaXMuY29uc3RydWN0b3JcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRoZW4oZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHByb21pc2UucmVzb2x2ZShjYWxsYmFjaygpKS50aGVuKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSwgZnVuY3Rpb24ocmVhc29uKSB7XG4gICAgICAgICAgICAgIHJldHVybiBwcm9taXNlLnJlc29sdmUoY2FsbGJhY2soKSkudGhlbihmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB0aHJvdyByZWFzb25cbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIEFwcC5tcFR5cGUgPSAncGFnZSdcbiAgICAgICAgQXBwLnJvdXRlID0gJ3BhZ2VzL2NyZWF0ZS1saXZlL2NyZWF0ZS1saXZlJ1xuICAgICAgICBBcHAuZWwgPSAnI3Jvb3QnXG4gICAgICAgIG5ldyBWdWUoQXBwKVxuICAgICAgICAiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///0\n");

/***/ }),
/* 1 */
/*!*****************************************************************************************!*\
  !*** /Users/caijizhou/Documents/HBuilderProjects/yoki-live/main.js?{"type":"appStyle"} ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Vue.prototype.__$appStyle__ = {}\nVue.prototype.__merge_style && Vue.prototype.__merge_style(__webpack_require__(/*! ./App.vue?vue&type=style&index=0&lang=scss */ 2).default,Vue.prototype.__$appStyle__)\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbbnVsbF0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0EsMkRBQTJELG1CQUFPLENBQUMsbURBQTRDIiwiZmlsZSI6IjEuanMiLCJzb3VyY2VzQ29udGVudCI6WyJWdWUucHJvdG90eXBlLl9fJGFwcFN0eWxlX18gPSB7fVxuVnVlLnByb3RvdHlwZS5fX21lcmdlX3N0eWxlICYmIFZ1ZS5wcm90b3R5cGUuX19tZXJnZV9zdHlsZShyZXF1aXJlKFwiLi9BcHAudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zY3NzXCIpLmRlZmF1bHQsVnVlLnByb3RvdHlwZS5fXyRhcHBTdHlsZV9fKVxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///1\n");

/***/ }),
/* 2 */
/*!******************************************************************************************************!*\
  !*** /Users/caijizhou/Documents/HBuilderProjects/yoki-live/App.vue?vue&type=style&index=0&lang=scss ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_style_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_9_oneOf_0_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_0_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_4_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-hbuilderx/packages/webpack-uni-nvue-loader/lib/style.js!../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--9-oneOf-0-1!../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/postcss-loader/src??ref--9-oneOf-0-2!../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/sass-loader/dist/cjs.js??ref--9-oneOf-0-3!../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--9-oneOf-0-4!../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./App.vue?vue&type=style&index=0&lang=scss */ 3);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_style_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_9_oneOf_0_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_0_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_4_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_style_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_9_oneOf_0_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_0_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_4_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_style_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_9_oneOf_0_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_0_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_4_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_style_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_9_oneOf_0_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_0_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_4_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_style_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_9_oneOf_0_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_0_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_9_oneOf_0_4_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss__WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),
/* 3 */
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-hbuilderx/packages/webpack-uni-nvue-loader/lib/style.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--9-oneOf-0-1!./node_modules/postcss-loader/src??ref--9-oneOf-0-2!./node_modules/sass-loader/dist/cjs.js??ref--9-oneOf-0-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--9-oneOf-0-4!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!/Users/caijizhou/Documents/HBuilderProjects/yoki-live/App.vue?vue&type=style&index=0&lang=scss ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  "iconfont": {
    "fontFamily": "iconfont"
  },
  "view": {
    "fontSize": "28rpx",
    "lineHeight": 1.8,
    "color": "#0E151D"
  },
  "text": {
    "fontSize": "28rpx",
    "lineHeight": 1.8,
    "color": "#0E151D"
  },
  "row": {
    "marginRight": "-20rpx",
    "marginLeft": "-20rpx",
    "flexWrap": "wrap",
    "flexDirection": "row"
  },
  "col-1": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "62.5rpx"
  },
  "col-2": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "125rpx"
  },
  "col-3": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "187.5rpx"
  },
  "col-4": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "250rpx"
  },
  "col-5": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "312.5rpx"
  },
  "col-6": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "375rpx"
  },
  "col-7": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "437.5rpx"
  },
  "col-8": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "500rpx"
  },
  "col-9": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "562.5rpx"
  },
  "col-10": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "625rpx"
  },
  "col-11": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "687.5rpx"
  },
  "col-12": {
    "position": "relative",
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx",
    "width": "750rpx"
  },
  "col-offset-12": {
    "marginLeft": "750rpx"
  },
  "col-offset-11": {
    "marginLeft": "687.5rpx"
  },
  "col-offset-10": {
    "marginLeft": "625rpx"
  },
  "col-offset-9": {
    "marginLeft": "562.5rpx"
  },
  "col-offset-8": {
    "marginLeft": "500rpx"
  },
  "col-offset-7": {
    "marginLeft": "437.5rpx"
  },
  "col-offset-6": {
    "marginLeft": "375rpx"
  },
  "col-offset-5": {
    "marginLeft": "312.5rpx"
  },
  "col-offset-4": {
    "marginLeft": "250rpx"
  },
  "col-offset-3": {
    "marginLeft": "187.5rpx"
  },
  "col-offset-2": {
    "marginLeft": "125rpx"
  },
  "col-offset-1": {
    "marginLeft": "62.5rpx"
  },
  "col-offset-0": {
    "marginLeft": 0
  },
  "flex": {
    "flexDirection": "row"
  },
  "flex-row": {
    "flexDirection": "row"
  },
  "flex-column": {
    "flexDirection": "column"
  },
  "flex-row-reverse": {
    "flexDirection": "row-reverse"
  },
  "flex-column-reverse": {
    "flexDirection": "column-reverse"
  },
  "flex-wrap": {
    "flexWrap": "wrap"
  },
  "flex-nowrap": {
    "flexWrap": "nowrap"
  },
  "justify-start": {
    "justifyContent": "flex-start"
  },
  "justify-end": {
    "justifyContent": "flex-end"
  },
  "justify-between": {
    "justifyContent": "space-between"
  },
  "justify-around": {
    "justifyContent": "space-around"
  },
  "justify-center": {
    "justifyContent": "center"
  },
  "align-center": {
    "alignItems": "center"
  },
  "align-stretch": {
    "alignItems": "stretch"
  },
  "align-start": {
    "alignItems": "flex-start"
  },
  "align-end": {
    "alignItems": "flex-end"
  },
  "flex-1": {
    "flex": 1
  },
  "flex-2": {
    "flex": 2
  },
  "flex-3": {
    "flex": 3
  },
  "flex-4": {
    "flex": 4
  },
  "flex-5": {
    "flex": 5
  },
  "container": {
    "paddingRight": "20rpx",
    "paddingLeft": "20rpx"
  },
  "m-0": {
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0
  },
  "m-1": {
    "marginTop": "10rpx",
    "marginRight": "10rpx",
    "marginBottom": "10rpx",
    "marginLeft": "10rpx"
  },
  "m-2": {
    "marginTop": "20rpx",
    "marginRight": "20rpx",
    "marginBottom": "20rpx",
    "marginLeft": "20rpx"
  },
  "m-3": {
    "marginTop": "30rpx",
    "marginRight": "30rpx",
    "marginBottom": "30rpx",
    "marginLeft": "30rpx"
  },
  "m-4": {
    "marginTop": "40rpx",
    "marginRight": "40rpx",
    "marginBottom": "40rpx",
    "marginLeft": "40rpx"
  },
  "m-5": {
    "marginTop": "50rpx",
    "marginRight": "50rpx",
    "marginBottom": "50rpx",
    "marginLeft": "50rpx"
  },
  "mt-0": {
    "marginTop": 0
  },
  "mt-1": {
    "marginTop": "10rpx"
  },
  "mt-2": {
    "marginTop": "20rpx"
  },
  "mt-3": {
    "marginTop": "30rpx"
  },
  "mt-4": {
    "marginTop": "40rpx"
  },
  "mt-5": {
    "marginTop": "50rpx"
  },
  "mb-0": {
    "marginBottom": 0
  },
  "mb-1": {
    "marginBottom": "10rpx"
  },
  "mb-2": {
    "marginBottom": "20rpx"
  },
  "mb-3": {
    "marginBottom": "30rpx"
  },
  "mb-4": {
    "marginBottom": "40rpx"
  },
  "mb-5": {
    "marginBottom": "50rpx"
  },
  "ml-0": {
    "marginLeft": 0
  },
  "ml-1": {
    "marginLeft": "10rpx"
  },
  "ml-2": {
    "marginLeft": "20rpx"
  },
  "ml-3": {
    "marginLeft": "30rpx"
  },
  "ml-4": {
    "marginLeft": "40rpx"
  },
  "ml-5": {
    "marginLeft": "50rpx"
  },
  "mr-0": {
    "marginRight": 0
  },
  "mr-1": {
    "marginRight": "10rpx"
  },
  "mr-2": {
    "marginRight": "20rpx"
  },
  "mr-3": {
    "marginRight": "30rpx"
  },
  "mr-4": {
    "marginRight": "40rpx"
  },
  "mr-5": {
    "marginRight": "50rpx"
  },
  "my-0": {
    "marginTop": 0,
    "marginBottom": 0
  },
  "my-1": {
    "marginTop": "10rpx",
    "marginBottom": "10rpx"
  },
  "my-2": {
    "marginTop": "20rpx",
    "marginBottom": "20rpx"
  },
  "my-3": {
    "marginTop": "30rpx",
    "marginBottom": "30rpx"
  },
  "my-4": {
    "marginTop": "40rpx",
    "marginBottom": "40rpx"
  },
  "my-5": {
    "marginTop": "50rpx",
    "marginBottom": "50rpx"
  },
  "mx-0": {
    "marginLeft": 0,
    "marginRight": 0
  },
  "mx-1": {
    "marginLeft": "10rpx",
    "marginRight": "10rpx"
  },
  "mx-2": {
    "marginLeft": "20rpx",
    "marginRight": "20rpx"
  },
  "mx-3": {
    "marginLeft": "30rpx",
    "marginRight": "30rpx"
  },
  "mx-4": {
    "marginLeft": "40rpx",
    "marginRight": "40rpx"
  },
  "mx-5": {
    "marginLeft": "50rpx",
    "marginRight": "50rpx"
  },
  "p-0": {
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0
  },
  "p": {
    "paddingTop": "5rpx",
    "paddingRight": "5rpx",
    "paddingBottom": "5rpx",
    "paddingLeft": "5rpx"
  },
  "p-1": {
    "paddingTop": "10rpx",
    "paddingRight": "10rpx",
    "paddingBottom": "10rpx",
    "paddingLeft": "10rpx"
  },
  "p-2": {
    "paddingTop": "20rpx",
    "paddingRight": "20rpx",
    "paddingBottom": "20rpx",
    "paddingLeft": "20rpx"
  },
  "p-3": {
    "paddingTop": "30rpx",
    "paddingRight": "30rpx",
    "paddingBottom": "30rpx",
    "paddingLeft": "30rpx"
  },
  "p-4": {
    "paddingTop": "40rpx",
    "paddingRight": "40rpx",
    "paddingBottom": "40rpx",
    "paddingLeft": "40rpx"
  },
  "p-5": {
    "paddingTop": "50rpx",
    "paddingRight": "50rpx",
    "paddingBottom": "50rpx",
    "paddingLeft": "50rpx"
  },
  "pt-0": {
    "paddingTop": 0
  },
  "pt": {
    "paddingTop": "5rpx"
  },
  "pt-1": {
    "paddingTop": "10rpx"
  },
  "pt-2": {
    "paddingTop": "20rpx"
  },
  "pt-3": {
    "paddingTop": "30rpx"
  },
  "pt-4": {
    "paddingTop": "40rpx"
  },
  "pt-5": {
    "paddingTop": "50rpx"
  },
  "pb-0": {
    "paddingBottom": 0
  },
  "pb-1": {
    "paddingBottom": "10rpx"
  },
  "pb": {
    "paddingBottom": "5rpx"
  },
  "pb-2": {
    "paddingBottom": "20rpx"
  },
  "pb-3": {
    "paddingBottom": "30rpx"
  },
  "pb-4": {
    "paddingBottom": "40rpx"
  },
  "pb-5": {
    "paddingBottom": "50rpx"
  },
  "pl-0": {
    "paddingLeft": 0
  },
  "pl": {
    "paddingLeft": "5rpx"
  },
  "pl-1": {
    "paddingLeft": "10rpx"
  },
  "pl-2": {
    "paddingLeft": "20rpx"
  },
  "pl-3": {
    "paddingLeft": "30rpx"
  },
  "pl-4": {
    "paddingLeft": "40rpx"
  },
  "pl-5": {
    "paddingLeft": "50rpx"
  },
  "pr-0": {
    "paddingRight": 0
  },
  "pr": {
    "paddingRight": "5rpx"
  },
  "pr-1": {
    "paddingRight": "10rpx"
  },
  "pr-2": {
    "paddingRight": "20rpx"
  },
  "pr-3": {
    "paddingRight": "30rpx"
  },
  "pr-4": {
    "paddingRight": "40rpx"
  },
  "pr-5": {
    "paddingRight": "50rpx"
  },
  "py-0": {
    "paddingTop": 0,
    "paddingBottom": 0
  },
  "py": {
    "paddingTop": "5rpx",
    "paddingBottom": "5rpx"
  },
  "py-1": {
    "paddingTop": "10rpx",
    "paddingBottom": "10rpx"
  },
  "py-2": {
    "paddingTop": "20rpx",
    "paddingBottom": "20rpx"
  },
  "py-3": {
    "paddingTop": "30rpx",
    "paddingBottom": "30rpx"
  },
  "py-4": {
    "paddingTop": "40rpx",
    "paddingBottom": "40rpx"
  },
  "py-5": {
    "paddingTop": "50rpx",
    "paddingBottom": "50rpx"
  },
  "px-0": {
    "paddingLeft": 0,
    "paddingRight": 0
  },
  "px-1": {
    "paddingLeft": "10rpx",
    "paddingRight": "10rpx"
  },
  "px": {
    "paddingLeft": "5rpx",
    "paddingRight": "5rpx"
  },
  "px-2": {
    "paddingLeft": "20rpx",
    "paddingRight": "20rpx"
  },
  "px-3": {
    "paddingLeft": "30rpx",
    "paddingRight": "30rpx"
  },
  "px-4": {
    "paddingLeft": "40rpx",
    "paddingRight": "40rpx"
  },
  "px-5": {
    "paddingLeft": "50rpx",
    "paddingRight": "50rpx"
  },
  "font-smaller": {
    "fontSize": "15rpx"
  },
  "font-small": {
    "fontSize": "20rpx"
  },
  "font-sm": {
    "fontSize": "25rpx"
  },
  "font": {
    "fontSize": "30rpx"
  },
  "font-md": {
    "fontSize": "35rpx"
  },
  "font-lg": {
    "fontSize": "40rpx"
  },
  "font-lgg": {
    "fontSize": "50rpx"
  },
  "h1": {
    "fontSize": "80rpx",
    "lineHeight": 1.8
  },
  "h2": {
    "fontSize": "60rpx",
    "lineHeight": 1.8
  },
  "h3": {
    "fontSize": "45rpx",
    "lineHeight": 1.8
  },
  "h4": {
    "fontSize": "32rpx",
    "lineHeight": 1.8
  },
  "h5": {
    "fontSize": "30rpx",
    "lineHeight": 1.8
  },
  "h6": {
    "fontSize": "28rpx",
    "lineHeight": 1.8
  },
  "text-through": {
    "textDecoration": "line-through"
  },
  "text-left": {
    "textAlign": "left"
  },
  "text-right": {
    "textAlign": "right"
  },
  "text-center": {
    "textAlign": "center"
  },
  "text-ellipsis": {
    "lines": 1
  },
  "font-weight-light": {
    "fontWeight": "300"
  },
  "font-weight-lighter": {
    "fontWeight": "100"
  },
  "font-weight-normal": {
    "fontWeight": "400"
  },
  "font-weight-bold": {
    "fontWeight": "700"
  },
  "font-weight-bolder": {
    "fontWeight": "bold"
  },
  "font-italic": {
    "fontStyle": "italic"
  },
  "text-white": {
    "color": "#ffffff"
  },
  "text-primary": {
    "color": "#007bff"
  },
  "text-hover-primary": {
    "color": "#0056b3"
  },
  "text-secondary": {
    "color": "#6c757d"
  },
  "text-hover-secondary": {
    "color": "#494f54"
  },
  "text-success": {
    "color": "#28a745"
  },
  "text-hover-success": {
    "color": "#19692c"
  },
  "text-info": {
    "color": "#17a2b8"
  },
  "text-hover-info": {
    "color": "#0f6674"
  },
  "text-warning": {
    "color": "#ffc107"
  },
  "text-hover-warning": {
    "color": "#ba8b00"
  },
  "text-danger": {
    "color": "#dc3545"
  },
  "text-hover-danger": {
    "color": "#a71d2a"
  },
  "text-light": {
    "color": "#f8f9fa"
  },
  "text-hover-light": {
    "color": "#cbd3da"
  },
  "text-dark": {
    "color": "#343a40"
  },
  "text-hover-dark": {
    "color": "#121416"
  },
  "text-body": {
    "color": "#212529"
  },
  "text-muted": {
    "color": "#6c757d"
  },
  "text-light-muted": {
    "color": "#A9A5A0"
  },
  "text-light-black": {
    "color": "rgba(0,0,0,0.5)"
  },
  "text-light-white": {
    "color": "rgba(255,255,255,0.5)"
  },
  "bg-primary": {
    "backgroundColor": "#007bff"
  },
  "bg-hover-primary": {
    "backgroundColor:hover": "#0062cc"
  },
  "bg-secondary": {
    "backgroundColor": "#6c757d"
  },
  "bg-hover-secondary": {
    "backgroundColor:hover": "#545b62"
  },
  "bg-success": {
    "backgroundColor": "#28a745"
  },
  "bg-hover-success": {
    "backgroundColor": "#1e7e34"
  },
  "bg-info": {
    "backgroundColor": "#17a2b8"
  },
  "bg-hover-info": {
    "backgroundColor": "#117a8b"
  },
  "bg-warning": {
    "backgroundColor": "#ffc107"
  },
  "bg-hover-warning": {
    "backgroundColor": "#d39e00"
  },
  "bg-danger": {
    "backgroundColor": "#dc3545"
  },
  "bg-hover-danger": {
    "backgroundColor": "#bd2130"
  },
  "bg-light": {
    "backgroundColor": "#f8f9fa"
  },
  "bg-hover-light": {
    "backgroundColor": "#dae0e5"
  },
  "bg-dark": {
    "backgroundColor": "#343a40"
  },
  "bg-hover-dark": {
    "backgroundColor": "#1d2124"
  },
  "bg-white": {
    "backgroundColor": "#ffffff"
  },
  "bg-transparent": {
    "backgroundColor": "rgba(0,0,0,0)"
  },
  "border": {
    "borderWidth": "1rpx",
    "borderStyle": "solid",
    "borderColor": "#dee2e6"
  },
  "border-top": {
    "borderTopWidth": "1rpx",
    "borderTopStyle": "solid",
    "borderTopColor": "#dee2e6"
  },
  "border-right": {
    "borderRightWidth": "1rpx",
    "borderRightStyle": "solid",
    "borderRightColor": "#dee2e6"
  },
  "border-bottom": {
    "borderBottomWidth": "1rpx",
    "borderBottomStyle": "solid",
    "borderBottomColor": "#dee2e6"
  },
  "border-left": {
    "borderLeftWidth": "1rpx",
    "borderLeftStyle": "solid",
    "borderLeftColor": "#dee2e6"
  },
  "border-0": {
    "borderWidth": 0
  },
  "border-top-0": {
    "borderTopWidth": 0
  },
  "border-right-0": {
    "borderRightWidth": 0
  },
  "border-bottom-0": {
    "borderBottomWidth": 0
  },
  "border-left-0": {
    "borderLeftWidth": 0
  },
  "border-primary": {
    "borderColor": "#007bff"
  },
  "border-secondary": {
    "borderColor": "#6c757d"
  },
  "border-light-secondary": {
    "borderColor": "#E9E8E5"
  },
  "border-success": {
    "borderColor": "#28a745"
  },
  "border-info": {
    "borderColor": "#17a2b8"
  },
  "border-warning": {
    "borderColor": "#ffc107"
  },
  "border-danger": {
    "borderColor": "#dc3545"
  },
  "border-light": {
    "borderColor": "#f8f9fa"
  },
  "border-dark": {
    "borderColor": "#343a40"
  },
  "border-white": {
    "borderColor": "#FFFFFF"
  },
  "rounded": {
    "borderRadius": "8rpx"
  },
  "rounded-lg": {
    "borderRadius": "14rpx"
  },
  "rounded-top": {
    "borderTopLeftRadius": "8rpx",
    "borderTopRightRadius": "8rpx"
  },
  "rounded-top-lg": {
    "borderTopLeftRadius": "14rpx",
    "borderTopRightRadius": "14rpx"
  },
  "rounded-right": {
    "borderTopRightRadius": "8rpx",
    "borderBottomRightRadius": "8rpx"
  },
  "rounded-bottom": {
    "borderBottomRightRadius": "8rpx",
    "borderBottomLeftRadius": "8rpx"
  },
  "rounded-bottom-lg": {
    "borderBottomRightRadius": "14rpx",
    "borderBottomLeftRadius": "14rpx"
  },
  "rounded-left": {
    "borderTopLeftRadius": "8rpx",
    "borderBottomLeftRadius": "8rpx"
  },
  "rounded-circle": {
    "borderRadius": "100rpx"
  },
  "rounded-0": {
    "borderRadius": 0
  },
  "overflow-hidden": {
    "overflow": "hidden"
  },
  "position-relative": {
    "position": "relative"
  },
  "position-absolute": {
    "position": "absolute"
  },
  "position-fixed": {
    "position": "fixed"
  },
  "fixed-top": {
    "position": "fixed",
    "top": 0,
    "right": 0,
    "left": 0,
    "zIndex": 1030
  },
  "fixed-bottom": {
    "position": "fixed",
    "right": 0,
    "bottom": 0,
    "left": 0,
    "zIndex": 1030
  },
  "top-0": {
    "top": 0
  },
  "left-0": {
    "left": 0
  },
  "right-0": {
    "right": 0
  },
  "bottom-0": {
    "bottom": 0
  },
  "mask": {
    "position": "fixed",
    "left": 0,
    "right": 0,
    "top": 0,
    "bottom": 0,
    "zIndex": 100
  },
  "text-main": {
    "color": "#8745FF"
  },
  "text-main-hover": {
    "color": "#BA7ACE"
  },
  "text-main-disabled": {
    "color": "#BA7ACE"
  },
  "bg-main": {
    "backgroundColor": "#8745FF"
  },
  "bg-main-hover": {
    "backgroundColor": "#BA7ACE"
  },
  "bg-main-disabled": {
    "backgroundColor": "#BA7ACE"
  },
  "border-main": {
    "borderColor": "#8745FF"
  },
  "btn-main-plain": {
    "borderColor": "#8745FF",
    "borderWidth": "1rpx",
    "borderStyle": "solid",
    "alignItems": "center",
    "justifyContent": "center",
    "color": "#8745FF",
    "paddingTop": "15rpx",
    "paddingBottom": "15rpx",
    "paddingLeft": "20rpx",
    "paddingRight": "20rpx",
    "borderRadius": "8rpx"
  },
  "line-h": {
    "lineHeight": 1.2
  },
  "f-divider": {
    "height": "18rpx",
    "backgroundColor": "#F5F5F4"
  },
  "u-relative": {
    "position": "relative"
  },
  "u-rela": {
    "position": "relative"
  },
  "u-absolute": {
    "position": "absolute"
  },
  "u-abso": {
    "position": "absolute"
  },
  "u-font-xs": {
    "fontSize": "22rpx"
  },
  "u-font-sm": {
    "fontSize": "26rpx"
  },
  "u-font-md": {
    "fontSize": "28rpx"
  },
  "u-font-lg": {
    "fontSize": "30rpx"
  },
  "u-font-xl": {
    "fontSize": "34rpx"
  },
  "u-flex": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "u-flex-wrap": {
    "flexWrap": "wrap"
  },
  "u-flex-nowrap": {
    "flexWrap": "nowrap"
  },
  "u-col-center": {
    "alignItems": "center"
  },
  "u-col-top": {
    "alignItems": "flex-start"
  },
  "u-col-bottom": {
    "alignItems": "flex-end"
  },
  "u-row-center": {
    "justifyContent": "center"
  },
  "u-row-left": {
    "justifyContent": "flex-start"
  },
  "u-row-right": {
    "justifyContent": "flex-end"
  },
  "u-row-between": {
    "justifyContent": "space-between"
  },
  "u-row-around": {
    "justifyContent": "space-around"
  },
  "u-text-left": {
    "textAlign": "left"
  },
  "u-text-center": {
    "textAlign": "center"
  },
  "u-text-right": {
    "textAlign": "right"
  },
  "u-flex-col": {
    "flexDirection": "column"
  },
  "u-flex-0": {
    "flex": 0
  },
  "u-flex-1": {
    "flex": 1
  },
  "u-flex-2": {
    "flex": 2
  },
  "u-flex-3": {
    "flex": 3
  },
  "u-flex-4": {
    "flex": 4
  },
  "u-flex-5": {
    "flex": 5
  },
  "u-flex-6": {
    "flex": 6
  },
  "u-flex-7": {
    "flex": 7
  },
  "u-flex-8": {
    "flex": 8
  },
  "u-flex-9": {
    "flex": 9
  },
  "u-flex-10": {
    "flex": 10
  },
  "u-flex-11": {
    "flex": 11
  },
  "u-flex-12": {
    "flex": 12
  },
  "u-font-9": {
    "fontSize": "9"
  },
  "u-font-10": {
    "fontSize": "10"
  },
  "u-font-11": {
    "fontSize": "11"
  },
  "u-font-12": {
    "fontSize": "12"
  },
  "u-font-13": {
    "fontSize": "13"
  },
  "u-font-14": {
    "fontSize": "14"
  },
  "u-font-15": {
    "fontSize": "15"
  },
  "u-font-16": {
    "fontSize": "16"
  },
  "u-font-17": {
    "fontSize": "17"
  },
  "u-font-18": {
    "fontSize": "18"
  },
  "u-font-19": {
    "fontSize": "19"
  },
  "u-font-20": {
    "fontSize": "20rpx"
  },
  "u-font-21": {
    "fontSize": "21rpx"
  },
  "u-font-22": {
    "fontSize": "22rpx"
  },
  "u-font-23": {
    "fontSize": "23rpx"
  },
  "u-font-24": {
    "fontSize": "24rpx"
  },
  "u-font-25": {
    "fontSize": "25rpx"
  },
  "u-font-26": {
    "fontSize": "26rpx"
  },
  "u-font-27": {
    "fontSize": "27rpx"
  },
  "u-font-28": {
    "fontSize": "28rpx"
  },
  "u-font-29": {
    "fontSize": "29rpx"
  },
  "u-font-30": {
    "fontSize": "30rpx"
  },
  "u-font-31": {
    "fontSize": "31rpx"
  },
  "u-font-32": {
    "fontSize": "32rpx"
  },
  "u-font-33": {
    "fontSize": "33rpx"
  },
  "u-font-34": {
    "fontSize": "34rpx"
  },
  "u-font-35": {
    "fontSize": "35rpx"
  },
  "u-font-36": {
    "fontSize": "36rpx"
  },
  "u-font-37": {
    "fontSize": "37rpx"
  },
  "u-font-38": {
    "fontSize": "38rpx"
  },
  "u-font-39": {
    "fontSize": "39rpx"
  },
  "u-font-40": {
    "fontSize": "40rpx"
  },
  "u-margin-0": {
    "marginTop": "0rpx",
    "marginRight": "0rpx",
    "marginBottom": "0rpx",
    "marginLeft": "0rpx"
  },
  "u-m-0": {
    "marginTop": "0rpx",
    "marginRight": "0rpx",
    "marginBottom": "0rpx",
    "marginLeft": "0rpx"
  },
  "u-padding-0": {
    "paddingTop": "0rpx",
    "paddingRight": "0rpx",
    "paddingBottom": "0rpx",
    "paddingLeft": "0rpx"
  },
  "u-p-0": {
    "paddingTop": "0rpx",
    "paddingRight": "0rpx",
    "paddingBottom": "0rpx",
    "paddingLeft": "0rpx"
  },
  "u-m-l-0": {
    "marginLeft": "0rpx"
  },
  "u-p-l-0": {
    "paddingLeft": "0rpx"
  },
  "u-margin-left-0": {
    "marginLeft": "0rpx"
  },
  "u-padding-left-0": {
    "paddingLeft": "0rpx"
  },
  "u-m-t-0": {
    "marginTop": "0rpx"
  },
  "u-p-t-0": {
    "paddingTop": "0rpx"
  },
  "u-margin-top-0": {
    "marginTop": "0rpx"
  },
  "u-padding-top-0": {
    "paddingTop": "0rpx"
  },
  "u-m-r-0": {
    "marginRight": "0rpx"
  },
  "u-p-r-0": {
    "paddingRight": "0rpx"
  },
  "u-margin-right-0": {
    "marginRight": "0rpx"
  },
  "u-padding-right-0": {
    "paddingRight": "0rpx"
  },
  "u-m-b-0": {
    "marginBottom": "0rpx"
  },
  "u-p-b-0": {
    "paddingBottom": "0rpx"
  },
  "u-margin-bottom-0": {
    "marginBottom": "0rpx"
  },
  "u-padding-bottom-0": {
    "paddingBottom": "0rpx"
  },
  "u-margin-2": {
    "marginTop": "2rpx",
    "marginRight": "2rpx",
    "marginBottom": "2rpx",
    "marginLeft": "2rpx"
  },
  "u-m-2": {
    "marginTop": "2rpx",
    "marginRight": "2rpx",
    "marginBottom": "2rpx",
    "marginLeft": "2rpx"
  },
  "u-padding-2": {
    "paddingTop": "2rpx",
    "paddingRight": "2rpx",
    "paddingBottom": "2rpx",
    "paddingLeft": "2rpx"
  },
  "u-p-2": {
    "paddingTop": "2rpx",
    "paddingRight": "2rpx",
    "paddingBottom": "2rpx",
    "paddingLeft": "2rpx"
  },
  "u-m-l-2": {
    "marginLeft": "2rpx"
  },
  "u-p-l-2": {
    "paddingLeft": "2rpx"
  },
  "u-margin-left-2": {
    "marginLeft": "2rpx"
  },
  "u-padding-left-2": {
    "paddingLeft": "2rpx"
  },
  "u-m-t-2": {
    "marginTop": "2rpx"
  },
  "u-p-t-2": {
    "paddingTop": "2rpx"
  },
  "u-margin-top-2": {
    "marginTop": "2rpx"
  },
  "u-padding-top-2": {
    "paddingTop": "2rpx"
  },
  "u-m-r-2": {
    "marginRight": "2rpx"
  },
  "u-p-r-2": {
    "paddingRight": "2rpx"
  },
  "u-margin-right-2": {
    "marginRight": "2rpx"
  },
  "u-padding-right-2": {
    "paddingRight": "2rpx"
  },
  "u-m-b-2": {
    "marginBottom": "2rpx"
  },
  "u-p-b-2": {
    "paddingBottom": "2rpx"
  },
  "u-margin-bottom-2": {
    "marginBottom": "2rpx"
  },
  "u-padding-bottom-2": {
    "paddingBottom": "2rpx"
  },
  "u-margin-4": {
    "marginTop": "4rpx",
    "marginRight": "4rpx",
    "marginBottom": "4rpx",
    "marginLeft": "4rpx"
  },
  "u-m-4": {
    "marginTop": "4rpx",
    "marginRight": "4rpx",
    "marginBottom": "4rpx",
    "marginLeft": "4rpx"
  },
  "u-padding-4": {
    "paddingTop": "4rpx",
    "paddingRight": "4rpx",
    "paddingBottom": "4rpx",
    "paddingLeft": "4rpx"
  },
  "u-p-4": {
    "paddingTop": "4rpx",
    "paddingRight": "4rpx",
    "paddingBottom": "4rpx",
    "paddingLeft": "4rpx"
  },
  "u-m-l-4": {
    "marginLeft": "4rpx"
  },
  "u-p-l-4": {
    "paddingLeft": "4rpx"
  },
  "u-margin-left-4": {
    "marginLeft": "4rpx"
  },
  "u-padding-left-4": {
    "paddingLeft": "4rpx"
  },
  "u-m-t-4": {
    "marginTop": "4rpx"
  },
  "u-p-t-4": {
    "paddingTop": "4rpx"
  },
  "u-margin-top-4": {
    "marginTop": "4rpx"
  },
  "u-padding-top-4": {
    "paddingTop": "4rpx"
  },
  "u-m-r-4": {
    "marginRight": "4rpx"
  },
  "u-p-r-4": {
    "paddingRight": "4rpx"
  },
  "u-margin-right-4": {
    "marginRight": "4rpx"
  },
  "u-padding-right-4": {
    "paddingRight": "4rpx"
  },
  "u-m-b-4": {
    "marginBottom": "4rpx"
  },
  "u-p-b-4": {
    "paddingBottom": "4rpx"
  },
  "u-margin-bottom-4": {
    "marginBottom": "4rpx"
  },
  "u-padding-bottom-4": {
    "paddingBottom": "4rpx"
  },
  "u-margin-5": {
    "marginTop": "5rpx",
    "marginRight": "5rpx",
    "marginBottom": "5rpx",
    "marginLeft": "5rpx"
  },
  "u-m-5": {
    "marginTop": "5rpx",
    "marginRight": "5rpx",
    "marginBottom": "5rpx",
    "marginLeft": "5rpx"
  },
  "u-padding-5": {
    "paddingTop": "5rpx",
    "paddingRight": "5rpx",
    "paddingBottom": "5rpx",
    "paddingLeft": "5rpx"
  },
  "u-p-5": {
    "paddingTop": "5rpx",
    "paddingRight": "5rpx",
    "paddingBottom": "5rpx",
    "paddingLeft": "5rpx"
  },
  "u-m-l-5": {
    "marginLeft": "5rpx"
  },
  "u-p-l-5": {
    "paddingLeft": "5rpx"
  },
  "u-margin-left-5": {
    "marginLeft": "5rpx"
  },
  "u-padding-left-5": {
    "paddingLeft": "5rpx"
  },
  "u-m-t-5": {
    "marginTop": "5rpx"
  },
  "u-p-t-5": {
    "paddingTop": "5rpx"
  },
  "u-margin-top-5": {
    "marginTop": "5rpx"
  },
  "u-padding-top-5": {
    "paddingTop": "5rpx"
  },
  "u-m-r-5": {
    "marginRight": "5rpx"
  },
  "u-p-r-5": {
    "paddingRight": "5rpx"
  },
  "u-margin-right-5": {
    "marginRight": "5rpx"
  },
  "u-padding-right-5": {
    "paddingRight": "5rpx"
  },
  "u-m-b-5": {
    "marginBottom": "5rpx"
  },
  "u-p-b-5": {
    "paddingBottom": "5rpx"
  },
  "u-margin-bottom-5": {
    "marginBottom": "5rpx"
  },
  "u-padding-bottom-5": {
    "paddingBottom": "5rpx"
  },
  "u-margin-6": {
    "marginTop": "6rpx",
    "marginRight": "6rpx",
    "marginBottom": "6rpx",
    "marginLeft": "6rpx"
  },
  "u-m-6": {
    "marginTop": "6rpx",
    "marginRight": "6rpx",
    "marginBottom": "6rpx",
    "marginLeft": "6rpx"
  },
  "u-padding-6": {
    "paddingTop": "6rpx",
    "paddingRight": "6rpx",
    "paddingBottom": "6rpx",
    "paddingLeft": "6rpx"
  },
  "u-p-6": {
    "paddingTop": "6rpx",
    "paddingRight": "6rpx",
    "paddingBottom": "6rpx",
    "paddingLeft": "6rpx"
  },
  "u-m-l-6": {
    "marginLeft": "6rpx"
  },
  "u-p-l-6": {
    "paddingLeft": "6rpx"
  },
  "u-margin-left-6": {
    "marginLeft": "6rpx"
  },
  "u-padding-left-6": {
    "paddingLeft": "6rpx"
  },
  "u-m-t-6": {
    "marginTop": "6rpx"
  },
  "u-p-t-6": {
    "paddingTop": "6rpx"
  },
  "u-margin-top-6": {
    "marginTop": "6rpx"
  },
  "u-padding-top-6": {
    "paddingTop": "6rpx"
  },
  "u-m-r-6": {
    "marginRight": "6rpx"
  },
  "u-p-r-6": {
    "paddingRight": "6rpx"
  },
  "u-margin-right-6": {
    "marginRight": "6rpx"
  },
  "u-padding-right-6": {
    "paddingRight": "6rpx"
  },
  "u-m-b-6": {
    "marginBottom": "6rpx"
  },
  "u-p-b-6": {
    "paddingBottom": "6rpx"
  },
  "u-margin-bottom-6": {
    "marginBottom": "6rpx"
  },
  "u-padding-bottom-6": {
    "paddingBottom": "6rpx"
  },
  "u-margin-8": {
    "marginTop": "8rpx",
    "marginRight": "8rpx",
    "marginBottom": "8rpx",
    "marginLeft": "8rpx"
  },
  "u-m-8": {
    "marginTop": "8rpx",
    "marginRight": "8rpx",
    "marginBottom": "8rpx",
    "marginLeft": "8rpx"
  },
  "u-padding-8": {
    "paddingTop": "8rpx",
    "paddingRight": "8rpx",
    "paddingBottom": "8rpx",
    "paddingLeft": "8rpx"
  },
  "u-p-8": {
    "paddingTop": "8rpx",
    "paddingRight": "8rpx",
    "paddingBottom": "8rpx",
    "paddingLeft": "8rpx"
  },
  "u-m-l-8": {
    "marginLeft": "8rpx"
  },
  "u-p-l-8": {
    "paddingLeft": "8rpx"
  },
  "u-margin-left-8": {
    "marginLeft": "8rpx"
  },
  "u-padding-left-8": {
    "paddingLeft": "8rpx"
  },
  "u-m-t-8": {
    "marginTop": "8rpx"
  },
  "u-p-t-8": {
    "paddingTop": "8rpx"
  },
  "u-margin-top-8": {
    "marginTop": "8rpx"
  },
  "u-padding-top-8": {
    "paddingTop": "8rpx"
  },
  "u-m-r-8": {
    "marginRight": "8rpx"
  },
  "u-p-r-8": {
    "paddingRight": "8rpx"
  },
  "u-margin-right-8": {
    "marginRight": "8rpx"
  },
  "u-padding-right-8": {
    "paddingRight": "8rpx"
  },
  "u-m-b-8": {
    "marginBottom": "8rpx"
  },
  "u-p-b-8": {
    "paddingBottom": "8rpx"
  },
  "u-margin-bottom-8": {
    "marginBottom": "8rpx"
  },
  "u-padding-bottom-8": {
    "paddingBottom": "8rpx"
  },
  "u-margin-10": {
    "marginTop": "10rpx",
    "marginRight": "10rpx",
    "marginBottom": "10rpx",
    "marginLeft": "10rpx"
  },
  "u-m-10": {
    "marginTop": "10rpx",
    "marginRight": "10rpx",
    "marginBottom": "10rpx",
    "marginLeft": "10rpx"
  },
  "u-padding-10": {
    "paddingTop": "10rpx",
    "paddingRight": "10rpx",
    "paddingBottom": "10rpx",
    "paddingLeft": "10rpx"
  },
  "u-p-10": {
    "paddingTop": "10rpx",
    "paddingRight": "10rpx",
    "paddingBottom": "10rpx",
    "paddingLeft": "10rpx"
  },
  "u-m-l-10": {
    "marginLeft": "10rpx"
  },
  "u-p-l-10": {
    "paddingLeft": "10rpx"
  },
  "u-margin-left-10": {
    "marginLeft": "10rpx"
  },
  "u-padding-left-10": {
    "paddingLeft": "10rpx"
  },
  "u-m-t-10": {
    "marginTop": "10rpx"
  },
  "u-p-t-10": {
    "paddingTop": "10rpx"
  },
  "u-margin-top-10": {
    "marginTop": "10rpx"
  },
  "u-padding-top-10": {
    "paddingTop": "10rpx"
  },
  "u-m-r-10": {
    "marginRight": "10rpx"
  },
  "u-p-r-10": {
    "paddingRight": "10rpx"
  },
  "u-margin-right-10": {
    "marginRight": "10rpx"
  },
  "u-padding-right-10": {
    "paddingRight": "10rpx"
  },
  "u-m-b-10": {
    "marginBottom": "10rpx"
  },
  "u-p-b-10": {
    "paddingBottom": "10rpx"
  },
  "u-margin-bottom-10": {
    "marginBottom": "10rpx"
  },
  "u-padding-bottom-10": {
    "paddingBottom": "10rpx"
  },
  "u-margin-12": {
    "marginTop": "12rpx",
    "marginRight": "12rpx",
    "marginBottom": "12rpx",
    "marginLeft": "12rpx"
  },
  "u-m-12": {
    "marginTop": "12rpx",
    "marginRight": "12rpx",
    "marginBottom": "12rpx",
    "marginLeft": "12rpx"
  },
  "u-padding-12": {
    "paddingTop": "12rpx",
    "paddingRight": "12rpx",
    "paddingBottom": "12rpx",
    "paddingLeft": "12rpx"
  },
  "u-p-12": {
    "paddingTop": "12rpx",
    "paddingRight": "12rpx",
    "paddingBottom": "12rpx",
    "paddingLeft": "12rpx"
  },
  "u-m-l-12": {
    "marginLeft": "12rpx"
  },
  "u-p-l-12": {
    "paddingLeft": "12rpx"
  },
  "u-margin-left-12": {
    "marginLeft": "12rpx"
  },
  "u-padding-left-12": {
    "paddingLeft": "12rpx"
  },
  "u-m-t-12": {
    "marginTop": "12rpx"
  },
  "u-p-t-12": {
    "paddingTop": "12rpx"
  },
  "u-margin-top-12": {
    "marginTop": "12rpx"
  },
  "u-padding-top-12": {
    "paddingTop": "12rpx"
  },
  "u-m-r-12": {
    "marginRight": "12rpx"
  },
  "u-p-r-12": {
    "paddingRight": "12rpx"
  },
  "u-margin-right-12": {
    "marginRight": "12rpx"
  },
  "u-padding-right-12": {
    "paddingRight": "12rpx"
  },
  "u-m-b-12": {
    "marginBottom": "12rpx"
  },
  "u-p-b-12": {
    "paddingBottom": "12rpx"
  },
  "u-margin-bottom-12": {
    "marginBottom": "12rpx"
  },
  "u-padding-bottom-12": {
    "paddingBottom": "12rpx"
  },
  "u-margin-14": {
    "marginTop": "14rpx",
    "marginRight": "14rpx",
    "marginBottom": "14rpx",
    "marginLeft": "14rpx"
  },
  "u-m-14": {
    "marginTop": "14rpx",
    "marginRight": "14rpx",
    "marginBottom": "14rpx",
    "marginLeft": "14rpx"
  },
  "u-padding-14": {
    "paddingTop": "14rpx",
    "paddingRight": "14rpx",
    "paddingBottom": "14rpx",
    "paddingLeft": "14rpx"
  },
  "u-p-14": {
    "paddingTop": "14rpx",
    "paddingRight": "14rpx",
    "paddingBottom": "14rpx",
    "paddingLeft": "14rpx"
  },
  "u-m-l-14": {
    "marginLeft": "14rpx"
  },
  "u-p-l-14": {
    "paddingLeft": "14rpx"
  },
  "u-margin-left-14": {
    "marginLeft": "14rpx"
  },
  "u-padding-left-14": {
    "paddingLeft": "14rpx"
  },
  "u-m-t-14": {
    "marginTop": "14rpx"
  },
  "u-p-t-14": {
    "paddingTop": "14rpx"
  },
  "u-margin-top-14": {
    "marginTop": "14rpx"
  },
  "u-padding-top-14": {
    "paddingTop": "14rpx"
  },
  "u-m-r-14": {
    "marginRight": "14rpx"
  },
  "u-p-r-14": {
    "paddingRight": "14rpx"
  },
  "u-margin-right-14": {
    "marginRight": "14rpx"
  },
  "u-padding-right-14": {
    "paddingRight": "14rpx"
  },
  "u-m-b-14": {
    "marginBottom": "14rpx"
  },
  "u-p-b-14": {
    "paddingBottom": "14rpx"
  },
  "u-margin-bottom-14": {
    "marginBottom": "14rpx"
  },
  "u-padding-bottom-14": {
    "paddingBottom": "14rpx"
  },
  "u-margin-15": {
    "marginTop": "15rpx",
    "marginRight": "15rpx",
    "marginBottom": "15rpx",
    "marginLeft": "15rpx"
  },
  "u-m-15": {
    "marginTop": "15rpx",
    "marginRight": "15rpx",
    "marginBottom": "15rpx",
    "marginLeft": "15rpx"
  },
  "u-padding-15": {
    "paddingTop": "15rpx",
    "paddingRight": "15rpx",
    "paddingBottom": "15rpx",
    "paddingLeft": "15rpx"
  },
  "u-p-15": {
    "paddingTop": "15rpx",
    "paddingRight": "15rpx",
    "paddingBottom": "15rpx",
    "paddingLeft": "15rpx"
  },
  "u-m-l-15": {
    "marginLeft": "15rpx"
  },
  "u-p-l-15": {
    "paddingLeft": "15rpx"
  },
  "u-margin-left-15": {
    "marginLeft": "15rpx"
  },
  "u-padding-left-15": {
    "paddingLeft": "15rpx"
  },
  "u-m-t-15": {
    "marginTop": "15rpx"
  },
  "u-p-t-15": {
    "paddingTop": "15rpx"
  },
  "u-margin-top-15": {
    "marginTop": "15rpx"
  },
  "u-padding-top-15": {
    "paddingTop": "15rpx"
  },
  "u-m-r-15": {
    "marginRight": "15rpx"
  },
  "u-p-r-15": {
    "paddingRight": "15rpx"
  },
  "u-margin-right-15": {
    "marginRight": "15rpx"
  },
  "u-padding-right-15": {
    "paddingRight": "15rpx"
  },
  "u-m-b-15": {
    "marginBottom": "15rpx"
  },
  "u-p-b-15": {
    "paddingBottom": "15rpx"
  },
  "u-margin-bottom-15": {
    "marginBottom": "15rpx"
  },
  "u-padding-bottom-15": {
    "paddingBottom": "15rpx"
  },
  "u-margin-16": {
    "marginTop": "16rpx",
    "marginRight": "16rpx",
    "marginBottom": "16rpx",
    "marginLeft": "16rpx"
  },
  "u-m-16": {
    "marginTop": "16rpx",
    "marginRight": "16rpx",
    "marginBottom": "16rpx",
    "marginLeft": "16rpx"
  },
  "u-padding-16": {
    "paddingTop": "16rpx",
    "paddingRight": "16rpx",
    "paddingBottom": "16rpx",
    "paddingLeft": "16rpx"
  },
  "u-p-16": {
    "paddingTop": "16rpx",
    "paddingRight": "16rpx",
    "paddingBottom": "16rpx",
    "paddingLeft": "16rpx"
  },
  "u-m-l-16": {
    "marginLeft": "16rpx"
  },
  "u-p-l-16": {
    "paddingLeft": "16rpx"
  },
  "u-margin-left-16": {
    "marginLeft": "16rpx"
  },
  "u-padding-left-16": {
    "paddingLeft": "16rpx"
  },
  "u-m-t-16": {
    "marginTop": "16rpx"
  },
  "u-p-t-16": {
    "paddingTop": "16rpx"
  },
  "u-margin-top-16": {
    "marginTop": "16rpx"
  },
  "u-padding-top-16": {
    "paddingTop": "16rpx"
  },
  "u-m-r-16": {
    "marginRight": "16rpx"
  },
  "u-p-r-16": {
    "paddingRight": "16rpx"
  },
  "u-margin-right-16": {
    "marginRight": "16rpx"
  },
  "u-padding-right-16": {
    "paddingRight": "16rpx"
  },
  "u-m-b-16": {
    "marginBottom": "16rpx"
  },
  "u-p-b-16": {
    "paddingBottom": "16rpx"
  },
  "u-margin-bottom-16": {
    "marginBottom": "16rpx"
  },
  "u-padding-bottom-16": {
    "paddingBottom": "16rpx"
  },
  "u-margin-18": {
    "marginTop": "18rpx",
    "marginRight": "18rpx",
    "marginBottom": "18rpx",
    "marginLeft": "18rpx"
  },
  "u-m-18": {
    "marginTop": "18rpx",
    "marginRight": "18rpx",
    "marginBottom": "18rpx",
    "marginLeft": "18rpx"
  },
  "u-padding-18": {
    "paddingTop": "18rpx",
    "paddingRight": "18rpx",
    "paddingBottom": "18rpx",
    "paddingLeft": "18rpx"
  },
  "u-p-18": {
    "paddingTop": "18rpx",
    "paddingRight": "18rpx",
    "paddingBottom": "18rpx",
    "paddingLeft": "18rpx"
  },
  "u-m-l-18": {
    "marginLeft": "18rpx"
  },
  "u-p-l-18": {
    "paddingLeft": "18rpx"
  },
  "u-margin-left-18": {
    "marginLeft": "18rpx"
  },
  "u-padding-left-18": {
    "paddingLeft": "18rpx"
  },
  "u-m-t-18": {
    "marginTop": "18rpx"
  },
  "u-p-t-18": {
    "paddingTop": "18rpx"
  },
  "u-margin-top-18": {
    "marginTop": "18rpx"
  },
  "u-padding-top-18": {
    "paddingTop": "18rpx"
  },
  "u-m-r-18": {
    "marginRight": "18rpx"
  },
  "u-p-r-18": {
    "paddingRight": "18rpx"
  },
  "u-margin-right-18": {
    "marginRight": "18rpx"
  },
  "u-padding-right-18": {
    "paddingRight": "18rpx"
  },
  "u-m-b-18": {
    "marginBottom": "18rpx"
  },
  "u-p-b-18": {
    "paddingBottom": "18rpx"
  },
  "u-margin-bottom-18": {
    "marginBottom": "18rpx"
  },
  "u-padding-bottom-18": {
    "paddingBottom": "18rpx"
  },
  "u-margin-20": {
    "marginTop": "20rpx",
    "marginRight": "20rpx",
    "marginBottom": "20rpx",
    "marginLeft": "20rpx"
  },
  "u-m-20": {
    "marginTop": "20rpx",
    "marginRight": "20rpx",
    "marginBottom": "20rpx",
    "marginLeft": "20rpx"
  },
  "u-padding-20": {
    "paddingTop": "20rpx",
    "paddingRight": "20rpx",
    "paddingBottom": "20rpx",
    "paddingLeft": "20rpx"
  },
  "u-p-20": {
    "paddingTop": "20rpx",
    "paddingRight": "20rpx",
    "paddingBottom": "20rpx",
    "paddingLeft": "20rpx"
  },
  "u-m-l-20": {
    "marginLeft": "20rpx"
  },
  "u-p-l-20": {
    "paddingLeft": "20rpx"
  },
  "u-margin-left-20": {
    "marginLeft": "20rpx"
  },
  "u-padding-left-20": {
    "paddingLeft": "20rpx"
  },
  "u-m-t-20": {
    "marginTop": "20rpx"
  },
  "u-p-t-20": {
    "paddingTop": "20rpx"
  },
  "u-margin-top-20": {
    "marginTop": "20rpx"
  },
  "u-padding-top-20": {
    "paddingTop": "20rpx"
  },
  "u-m-r-20": {
    "marginRight": "20rpx"
  },
  "u-p-r-20": {
    "paddingRight": "20rpx"
  },
  "u-margin-right-20": {
    "marginRight": "20rpx"
  },
  "u-padding-right-20": {
    "paddingRight": "20rpx"
  },
  "u-m-b-20": {
    "marginBottom": "20rpx"
  },
  "u-p-b-20": {
    "paddingBottom": "20rpx"
  },
  "u-margin-bottom-20": {
    "marginBottom": "20rpx"
  },
  "u-padding-bottom-20": {
    "paddingBottom": "20rpx"
  },
  "u-margin-22": {
    "marginTop": "22rpx",
    "marginRight": "22rpx",
    "marginBottom": "22rpx",
    "marginLeft": "22rpx"
  },
  "u-m-22": {
    "marginTop": "22rpx",
    "marginRight": "22rpx",
    "marginBottom": "22rpx",
    "marginLeft": "22rpx"
  },
  "u-padding-22": {
    "paddingTop": "22rpx",
    "paddingRight": "22rpx",
    "paddingBottom": "22rpx",
    "paddingLeft": "22rpx"
  },
  "u-p-22": {
    "paddingTop": "22rpx",
    "paddingRight": "22rpx",
    "paddingBottom": "22rpx",
    "paddingLeft": "22rpx"
  },
  "u-m-l-22": {
    "marginLeft": "22rpx"
  },
  "u-p-l-22": {
    "paddingLeft": "22rpx"
  },
  "u-margin-left-22": {
    "marginLeft": "22rpx"
  },
  "u-padding-left-22": {
    "paddingLeft": "22rpx"
  },
  "u-m-t-22": {
    "marginTop": "22rpx"
  },
  "u-p-t-22": {
    "paddingTop": "22rpx"
  },
  "u-margin-top-22": {
    "marginTop": "22rpx"
  },
  "u-padding-top-22": {
    "paddingTop": "22rpx"
  },
  "u-m-r-22": {
    "marginRight": "22rpx"
  },
  "u-p-r-22": {
    "paddingRight": "22rpx"
  },
  "u-margin-right-22": {
    "marginRight": "22rpx"
  },
  "u-padding-right-22": {
    "paddingRight": "22rpx"
  },
  "u-m-b-22": {
    "marginBottom": "22rpx"
  },
  "u-p-b-22": {
    "paddingBottom": "22rpx"
  },
  "u-margin-bottom-22": {
    "marginBottom": "22rpx"
  },
  "u-padding-bottom-22": {
    "paddingBottom": "22rpx"
  },
  "u-margin-24": {
    "marginTop": "24rpx",
    "marginRight": "24rpx",
    "marginBottom": "24rpx",
    "marginLeft": "24rpx"
  },
  "u-m-24": {
    "marginTop": "24rpx",
    "marginRight": "24rpx",
    "marginBottom": "24rpx",
    "marginLeft": "24rpx"
  },
  "u-padding-24": {
    "paddingTop": "24rpx",
    "paddingRight": "24rpx",
    "paddingBottom": "24rpx",
    "paddingLeft": "24rpx"
  },
  "u-p-24": {
    "paddingTop": "24rpx",
    "paddingRight": "24rpx",
    "paddingBottom": "24rpx",
    "paddingLeft": "24rpx"
  },
  "u-m-l-24": {
    "marginLeft": "24rpx"
  },
  "u-p-l-24": {
    "paddingLeft": "24rpx"
  },
  "u-margin-left-24": {
    "marginLeft": "24rpx"
  },
  "u-padding-left-24": {
    "paddingLeft": "24rpx"
  },
  "u-m-t-24": {
    "marginTop": "24rpx"
  },
  "u-p-t-24": {
    "paddingTop": "24rpx"
  },
  "u-margin-top-24": {
    "marginTop": "24rpx"
  },
  "u-padding-top-24": {
    "paddingTop": "24rpx"
  },
  "u-m-r-24": {
    "marginRight": "24rpx"
  },
  "u-p-r-24": {
    "paddingRight": "24rpx"
  },
  "u-margin-right-24": {
    "marginRight": "24rpx"
  },
  "u-padding-right-24": {
    "paddingRight": "24rpx"
  },
  "u-m-b-24": {
    "marginBottom": "24rpx"
  },
  "u-p-b-24": {
    "paddingBottom": "24rpx"
  },
  "u-margin-bottom-24": {
    "marginBottom": "24rpx"
  },
  "u-padding-bottom-24": {
    "paddingBottom": "24rpx"
  },
  "u-margin-25": {
    "marginTop": "25rpx",
    "marginRight": "25rpx",
    "marginBottom": "25rpx",
    "marginLeft": "25rpx"
  },
  "u-m-25": {
    "marginTop": "25rpx",
    "marginRight": "25rpx",
    "marginBottom": "25rpx",
    "marginLeft": "25rpx"
  },
  "u-padding-25": {
    "paddingTop": "25rpx",
    "paddingRight": "25rpx",
    "paddingBottom": "25rpx",
    "paddingLeft": "25rpx"
  },
  "u-p-25": {
    "paddingTop": "25rpx",
    "paddingRight": "25rpx",
    "paddingBottom": "25rpx",
    "paddingLeft": "25rpx"
  },
  "u-m-l-25": {
    "marginLeft": "25rpx"
  },
  "u-p-l-25": {
    "paddingLeft": "25rpx"
  },
  "u-margin-left-25": {
    "marginLeft": "25rpx"
  },
  "u-padding-left-25": {
    "paddingLeft": "25rpx"
  },
  "u-m-t-25": {
    "marginTop": "25rpx"
  },
  "u-p-t-25": {
    "paddingTop": "25rpx"
  },
  "u-margin-top-25": {
    "marginTop": "25rpx"
  },
  "u-padding-top-25": {
    "paddingTop": "25rpx"
  },
  "u-m-r-25": {
    "marginRight": "25rpx"
  },
  "u-p-r-25": {
    "paddingRight": "25rpx"
  },
  "u-margin-right-25": {
    "marginRight": "25rpx"
  },
  "u-padding-right-25": {
    "paddingRight": "25rpx"
  },
  "u-m-b-25": {
    "marginBottom": "25rpx"
  },
  "u-p-b-25": {
    "paddingBottom": "25rpx"
  },
  "u-margin-bottom-25": {
    "marginBottom": "25rpx"
  },
  "u-padding-bottom-25": {
    "paddingBottom": "25rpx"
  },
  "u-margin-26": {
    "marginTop": "26rpx",
    "marginRight": "26rpx",
    "marginBottom": "26rpx",
    "marginLeft": "26rpx"
  },
  "u-m-26": {
    "marginTop": "26rpx",
    "marginRight": "26rpx",
    "marginBottom": "26rpx",
    "marginLeft": "26rpx"
  },
  "u-padding-26": {
    "paddingTop": "26rpx",
    "paddingRight": "26rpx",
    "paddingBottom": "26rpx",
    "paddingLeft": "26rpx"
  },
  "u-p-26": {
    "paddingTop": "26rpx",
    "paddingRight": "26rpx",
    "paddingBottom": "26rpx",
    "paddingLeft": "26rpx"
  },
  "u-m-l-26": {
    "marginLeft": "26rpx"
  },
  "u-p-l-26": {
    "paddingLeft": "26rpx"
  },
  "u-margin-left-26": {
    "marginLeft": "26rpx"
  },
  "u-padding-left-26": {
    "paddingLeft": "26rpx"
  },
  "u-m-t-26": {
    "marginTop": "26rpx"
  },
  "u-p-t-26": {
    "paddingTop": "26rpx"
  },
  "u-margin-top-26": {
    "marginTop": "26rpx"
  },
  "u-padding-top-26": {
    "paddingTop": "26rpx"
  },
  "u-m-r-26": {
    "marginRight": "26rpx"
  },
  "u-p-r-26": {
    "paddingRight": "26rpx"
  },
  "u-margin-right-26": {
    "marginRight": "26rpx"
  },
  "u-padding-right-26": {
    "paddingRight": "26rpx"
  },
  "u-m-b-26": {
    "marginBottom": "26rpx"
  },
  "u-p-b-26": {
    "paddingBottom": "26rpx"
  },
  "u-margin-bottom-26": {
    "marginBottom": "26rpx"
  },
  "u-padding-bottom-26": {
    "paddingBottom": "26rpx"
  },
  "u-margin-28": {
    "marginTop": "28rpx",
    "marginRight": "28rpx",
    "marginBottom": "28rpx",
    "marginLeft": "28rpx"
  },
  "u-m-28": {
    "marginTop": "28rpx",
    "marginRight": "28rpx",
    "marginBottom": "28rpx",
    "marginLeft": "28rpx"
  },
  "u-padding-28": {
    "paddingTop": "28rpx",
    "paddingRight": "28rpx",
    "paddingBottom": "28rpx",
    "paddingLeft": "28rpx"
  },
  "u-p-28": {
    "paddingTop": "28rpx",
    "paddingRight": "28rpx",
    "paddingBottom": "28rpx",
    "paddingLeft": "28rpx"
  },
  "u-m-l-28": {
    "marginLeft": "28rpx"
  },
  "u-p-l-28": {
    "paddingLeft": "28rpx"
  },
  "u-margin-left-28": {
    "marginLeft": "28rpx"
  },
  "u-padding-left-28": {
    "paddingLeft": "28rpx"
  },
  "u-m-t-28": {
    "marginTop": "28rpx"
  },
  "u-p-t-28": {
    "paddingTop": "28rpx"
  },
  "u-margin-top-28": {
    "marginTop": "28rpx"
  },
  "u-padding-top-28": {
    "paddingTop": "28rpx"
  },
  "u-m-r-28": {
    "marginRight": "28rpx"
  },
  "u-p-r-28": {
    "paddingRight": "28rpx"
  },
  "u-margin-right-28": {
    "marginRight": "28rpx"
  },
  "u-padding-right-28": {
    "paddingRight": "28rpx"
  },
  "u-m-b-28": {
    "marginBottom": "28rpx"
  },
  "u-p-b-28": {
    "paddingBottom": "28rpx"
  },
  "u-margin-bottom-28": {
    "marginBottom": "28rpx"
  },
  "u-padding-bottom-28": {
    "paddingBottom": "28rpx"
  },
  "u-margin-30": {
    "marginTop": "30rpx",
    "marginRight": "30rpx",
    "marginBottom": "30rpx",
    "marginLeft": "30rpx"
  },
  "u-m-30": {
    "marginTop": "30rpx",
    "marginRight": "30rpx",
    "marginBottom": "30rpx",
    "marginLeft": "30rpx"
  },
  "u-padding-30": {
    "paddingTop": "30rpx",
    "paddingRight": "30rpx",
    "paddingBottom": "30rpx",
    "paddingLeft": "30rpx"
  },
  "u-p-30": {
    "paddingTop": "30rpx",
    "paddingRight": "30rpx",
    "paddingBottom": "30rpx",
    "paddingLeft": "30rpx"
  },
  "u-m-l-30": {
    "marginLeft": "30rpx"
  },
  "u-p-l-30": {
    "paddingLeft": "30rpx"
  },
  "u-margin-left-30": {
    "marginLeft": "30rpx"
  },
  "u-padding-left-30": {
    "paddingLeft": "30rpx"
  },
  "u-m-t-30": {
    "marginTop": "30rpx"
  },
  "u-p-t-30": {
    "paddingTop": "30rpx"
  },
  "u-margin-top-30": {
    "marginTop": "30rpx"
  },
  "u-padding-top-30": {
    "paddingTop": "30rpx"
  },
  "u-m-r-30": {
    "marginRight": "30rpx"
  },
  "u-p-r-30": {
    "paddingRight": "30rpx"
  },
  "u-margin-right-30": {
    "marginRight": "30rpx"
  },
  "u-padding-right-30": {
    "paddingRight": "30rpx"
  },
  "u-m-b-30": {
    "marginBottom": "30rpx"
  },
  "u-p-b-30": {
    "paddingBottom": "30rpx"
  },
  "u-margin-bottom-30": {
    "marginBottom": "30rpx"
  },
  "u-padding-bottom-30": {
    "paddingBottom": "30rpx"
  },
  "u-margin-32": {
    "marginTop": "32rpx",
    "marginRight": "32rpx",
    "marginBottom": "32rpx",
    "marginLeft": "32rpx"
  },
  "u-m-32": {
    "marginTop": "32rpx",
    "marginRight": "32rpx",
    "marginBottom": "32rpx",
    "marginLeft": "32rpx"
  },
  "u-padding-32": {
    "paddingTop": "32rpx",
    "paddingRight": "32rpx",
    "paddingBottom": "32rpx",
    "paddingLeft": "32rpx"
  },
  "u-p-32": {
    "paddingTop": "32rpx",
    "paddingRight": "32rpx",
    "paddingBottom": "32rpx",
    "paddingLeft": "32rpx"
  },
  "u-m-l-32": {
    "marginLeft": "32rpx"
  },
  "u-p-l-32": {
    "paddingLeft": "32rpx"
  },
  "u-margin-left-32": {
    "marginLeft": "32rpx"
  },
  "u-padding-left-32": {
    "paddingLeft": "32rpx"
  },
  "u-m-t-32": {
    "marginTop": "32rpx"
  },
  "u-p-t-32": {
    "paddingTop": "32rpx"
  },
  "u-margin-top-32": {
    "marginTop": "32rpx"
  },
  "u-padding-top-32": {
    "paddingTop": "32rpx"
  },
  "u-m-r-32": {
    "marginRight": "32rpx"
  },
  "u-p-r-32": {
    "paddingRight": "32rpx"
  },
  "u-margin-right-32": {
    "marginRight": "32rpx"
  },
  "u-padding-right-32": {
    "paddingRight": "32rpx"
  },
  "u-m-b-32": {
    "marginBottom": "32rpx"
  },
  "u-p-b-32": {
    "paddingBottom": "32rpx"
  },
  "u-margin-bottom-32": {
    "marginBottom": "32rpx"
  },
  "u-padding-bottom-32": {
    "paddingBottom": "32rpx"
  },
  "u-margin-34": {
    "marginTop": "34rpx",
    "marginRight": "34rpx",
    "marginBottom": "34rpx",
    "marginLeft": "34rpx"
  },
  "u-m-34": {
    "marginTop": "34rpx",
    "marginRight": "34rpx",
    "marginBottom": "34rpx",
    "marginLeft": "34rpx"
  },
  "u-padding-34": {
    "paddingTop": "34rpx",
    "paddingRight": "34rpx",
    "paddingBottom": "34rpx",
    "paddingLeft": "34rpx"
  },
  "u-p-34": {
    "paddingTop": "34rpx",
    "paddingRight": "34rpx",
    "paddingBottom": "34rpx",
    "paddingLeft": "34rpx"
  },
  "u-m-l-34": {
    "marginLeft": "34rpx"
  },
  "u-p-l-34": {
    "paddingLeft": "34rpx"
  },
  "u-margin-left-34": {
    "marginLeft": "34rpx"
  },
  "u-padding-left-34": {
    "paddingLeft": "34rpx"
  },
  "u-m-t-34": {
    "marginTop": "34rpx"
  },
  "u-p-t-34": {
    "paddingTop": "34rpx"
  },
  "u-margin-top-34": {
    "marginTop": "34rpx"
  },
  "u-padding-top-34": {
    "paddingTop": "34rpx"
  },
  "u-m-r-34": {
    "marginRight": "34rpx"
  },
  "u-p-r-34": {
    "paddingRight": "34rpx"
  },
  "u-margin-right-34": {
    "marginRight": "34rpx"
  },
  "u-padding-right-34": {
    "paddingRight": "34rpx"
  },
  "u-m-b-34": {
    "marginBottom": "34rpx"
  },
  "u-p-b-34": {
    "paddingBottom": "34rpx"
  },
  "u-margin-bottom-34": {
    "marginBottom": "34rpx"
  },
  "u-padding-bottom-34": {
    "paddingBottom": "34rpx"
  },
  "u-margin-35": {
    "marginTop": "35rpx",
    "marginRight": "35rpx",
    "marginBottom": "35rpx",
    "marginLeft": "35rpx"
  },
  "u-m-35": {
    "marginTop": "35rpx",
    "marginRight": "35rpx",
    "marginBottom": "35rpx",
    "marginLeft": "35rpx"
  },
  "u-padding-35": {
    "paddingTop": "35rpx",
    "paddingRight": "35rpx",
    "paddingBottom": "35rpx",
    "paddingLeft": "35rpx"
  },
  "u-p-35": {
    "paddingTop": "35rpx",
    "paddingRight": "35rpx",
    "paddingBottom": "35rpx",
    "paddingLeft": "35rpx"
  },
  "u-m-l-35": {
    "marginLeft": "35rpx"
  },
  "u-p-l-35": {
    "paddingLeft": "35rpx"
  },
  "u-margin-left-35": {
    "marginLeft": "35rpx"
  },
  "u-padding-left-35": {
    "paddingLeft": "35rpx"
  },
  "u-m-t-35": {
    "marginTop": "35rpx"
  },
  "u-p-t-35": {
    "paddingTop": "35rpx"
  },
  "u-margin-top-35": {
    "marginTop": "35rpx"
  },
  "u-padding-top-35": {
    "paddingTop": "35rpx"
  },
  "u-m-r-35": {
    "marginRight": "35rpx"
  },
  "u-p-r-35": {
    "paddingRight": "35rpx"
  },
  "u-margin-right-35": {
    "marginRight": "35rpx"
  },
  "u-padding-right-35": {
    "paddingRight": "35rpx"
  },
  "u-m-b-35": {
    "marginBottom": "35rpx"
  },
  "u-p-b-35": {
    "paddingBottom": "35rpx"
  },
  "u-margin-bottom-35": {
    "marginBottom": "35rpx"
  },
  "u-padding-bottom-35": {
    "paddingBottom": "35rpx"
  },
  "u-margin-36": {
    "marginTop": "36rpx",
    "marginRight": "36rpx",
    "marginBottom": "36rpx",
    "marginLeft": "36rpx"
  },
  "u-m-36": {
    "marginTop": "36rpx",
    "marginRight": "36rpx",
    "marginBottom": "36rpx",
    "marginLeft": "36rpx"
  },
  "u-padding-36": {
    "paddingTop": "36rpx",
    "paddingRight": "36rpx",
    "paddingBottom": "36rpx",
    "paddingLeft": "36rpx"
  },
  "u-p-36": {
    "paddingTop": "36rpx",
    "paddingRight": "36rpx",
    "paddingBottom": "36rpx",
    "paddingLeft": "36rpx"
  },
  "u-m-l-36": {
    "marginLeft": "36rpx"
  },
  "u-p-l-36": {
    "paddingLeft": "36rpx"
  },
  "u-margin-left-36": {
    "marginLeft": "36rpx"
  },
  "u-padding-left-36": {
    "paddingLeft": "36rpx"
  },
  "u-m-t-36": {
    "marginTop": "36rpx"
  },
  "u-p-t-36": {
    "paddingTop": "36rpx"
  },
  "u-margin-top-36": {
    "marginTop": "36rpx"
  },
  "u-padding-top-36": {
    "paddingTop": "36rpx"
  },
  "u-m-r-36": {
    "marginRight": "36rpx"
  },
  "u-p-r-36": {
    "paddingRight": "36rpx"
  },
  "u-margin-right-36": {
    "marginRight": "36rpx"
  },
  "u-padding-right-36": {
    "paddingRight": "36rpx"
  },
  "u-m-b-36": {
    "marginBottom": "36rpx"
  },
  "u-p-b-36": {
    "paddingBottom": "36rpx"
  },
  "u-margin-bottom-36": {
    "marginBottom": "36rpx"
  },
  "u-padding-bottom-36": {
    "paddingBottom": "36rpx"
  },
  "u-margin-38": {
    "marginTop": "38rpx",
    "marginRight": "38rpx",
    "marginBottom": "38rpx",
    "marginLeft": "38rpx"
  },
  "u-m-38": {
    "marginTop": "38rpx",
    "marginRight": "38rpx",
    "marginBottom": "38rpx",
    "marginLeft": "38rpx"
  },
  "u-padding-38": {
    "paddingTop": "38rpx",
    "paddingRight": "38rpx",
    "paddingBottom": "38rpx",
    "paddingLeft": "38rpx"
  },
  "u-p-38": {
    "paddingTop": "38rpx",
    "paddingRight": "38rpx",
    "paddingBottom": "38rpx",
    "paddingLeft": "38rpx"
  },
  "u-m-l-38": {
    "marginLeft": "38rpx"
  },
  "u-p-l-38": {
    "paddingLeft": "38rpx"
  },
  "u-margin-left-38": {
    "marginLeft": "38rpx"
  },
  "u-padding-left-38": {
    "paddingLeft": "38rpx"
  },
  "u-m-t-38": {
    "marginTop": "38rpx"
  },
  "u-p-t-38": {
    "paddingTop": "38rpx"
  },
  "u-margin-top-38": {
    "marginTop": "38rpx"
  },
  "u-padding-top-38": {
    "paddingTop": "38rpx"
  },
  "u-m-r-38": {
    "marginRight": "38rpx"
  },
  "u-p-r-38": {
    "paddingRight": "38rpx"
  },
  "u-margin-right-38": {
    "marginRight": "38rpx"
  },
  "u-padding-right-38": {
    "paddingRight": "38rpx"
  },
  "u-m-b-38": {
    "marginBottom": "38rpx"
  },
  "u-p-b-38": {
    "paddingBottom": "38rpx"
  },
  "u-margin-bottom-38": {
    "marginBottom": "38rpx"
  },
  "u-padding-bottom-38": {
    "paddingBottom": "38rpx"
  },
  "u-margin-40": {
    "marginTop": "40rpx",
    "marginRight": "40rpx",
    "marginBottom": "40rpx",
    "marginLeft": "40rpx"
  },
  "u-m-40": {
    "marginTop": "40rpx",
    "marginRight": "40rpx",
    "marginBottom": "40rpx",
    "marginLeft": "40rpx"
  },
  "u-padding-40": {
    "paddingTop": "40rpx",
    "paddingRight": "40rpx",
    "paddingBottom": "40rpx",
    "paddingLeft": "40rpx"
  },
  "u-p-40": {
    "paddingTop": "40rpx",
    "paddingRight": "40rpx",
    "paddingBottom": "40rpx",
    "paddingLeft": "40rpx"
  },
  "u-m-l-40": {
    "marginLeft": "40rpx"
  },
  "u-p-l-40": {
    "paddingLeft": "40rpx"
  },
  "u-margin-left-40": {
    "marginLeft": "40rpx"
  },
  "u-padding-left-40": {
    "paddingLeft": "40rpx"
  },
  "u-m-t-40": {
    "marginTop": "40rpx"
  },
  "u-p-t-40": {
    "paddingTop": "40rpx"
  },
  "u-margin-top-40": {
    "marginTop": "40rpx"
  },
  "u-padding-top-40": {
    "paddingTop": "40rpx"
  },
  "u-m-r-40": {
    "marginRight": "40rpx"
  },
  "u-p-r-40": {
    "paddingRight": "40rpx"
  },
  "u-margin-right-40": {
    "marginRight": "40rpx"
  },
  "u-padding-right-40": {
    "paddingRight": "40rpx"
  },
  "u-m-b-40": {
    "marginBottom": "40rpx"
  },
  "u-p-b-40": {
    "paddingBottom": "40rpx"
  },
  "u-margin-bottom-40": {
    "marginBottom": "40rpx"
  },
  "u-padding-bottom-40": {
    "paddingBottom": "40rpx"
  },
  "u-margin-42": {
    "marginTop": "42rpx",
    "marginRight": "42rpx",
    "marginBottom": "42rpx",
    "marginLeft": "42rpx"
  },
  "u-m-42": {
    "marginTop": "42rpx",
    "marginRight": "42rpx",
    "marginBottom": "42rpx",
    "marginLeft": "42rpx"
  },
  "u-padding-42": {
    "paddingTop": "42rpx",
    "paddingRight": "42rpx",
    "paddingBottom": "42rpx",
    "paddingLeft": "42rpx"
  },
  "u-p-42": {
    "paddingTop": "42rpx",
    "paddingRight": "42rpx",
    "paddingBottom": "42rpx",
    "paddingLeft": "42rpx"
  },
  "u-m-l-42": {
    "marginLeft": "42rpx"
  },
  "u-p-l-42": {
    "paddingLeft": "42rpx"
  },
  "u-margin-left-42": {
    "marginLeft": "42rpx"
  },
  "u-padding-left-42": {
    "paddingLeft": "42rpx"
  },
  "u-m-t-42": {
    "marginTop": "42rpx"
  },
  "u-p-t-42": {
    "paddingTop": "42rpx"
  },
  "u-margin-top-42": {
    "marginTop": "42rpx"
  },
  "u-padding-top-42": {
    "paddingTop": "42rpx"
  },
  "u-m-r-42": {
    "marginRight": "42rpx"
  },
  "u-p-r-42": {
    "paddingRight": "42rpx"
  },
  "u-margin-right-42": {
    "marginRight": "42rpx"
  },
  "u-padding-right-42": {
    "paddingRight": "42rpx"
  },
  "u-m-b-42": {
    "marginBottom": "42rpx"
  },
  "u-p-b-42": {
    "paddingBottom": "42rpx"
  },
  "u-margin-bottom-42": {
    "marginBottom": "42rpx"
  },
  "u-padding-bottom-42": {
    "paddingBottom": "42rpx"
  },
  "u-margin-44": {
    "marginTop": "44rpx",
    "marginRight": "44rpx",
    "marginBottom": "44rpx",
    "marginLeft": "44rpx"
  },
  "u-m-44": {
    "marginTop": "44rpx",
    "marginRight": "44rpx",
    "marginBottom": "44rpx",
    "marginLeft": "44rpx"
  },
  "u-padding-44": {
    "paddingTop": "44rpx",
    "paddingRight": "44rpx",
    "paddingBottom": "44rpx",
    "paddingLeft": "44rpx"
  },
  "u-p-44": {
    "paddingTop": "44rpx",
    "paddingRight": "44rpx",
    "paddingBottom": "44rpx",
    "paddingLeft": "44rpx"
  },
  "u-m-l-44": {
    "marginLeft": "44rpx"
  },
  "u-p-l-44": {
    "paddingLeft": "44rpx"
  },
  "u-margin-left-44": {
    "marginLeft": "44rpx"
  },
  "u-padding-left-44": {
    "paddingLeft": "44rpx"
  },
  "u-m-t-44": {
    "marginTop": "44rpx"
  },
  "u-p-t-44": {
    "paddingTop": "44rpx"
  },
  "u-margin-top-44": {
    "marginTop": "44rpx"
  },
  "u-padding-top-44": {
    "paddingTop": "44rpx"
  },
  "u-m-r-44": {
    "marginRight": "44rpx"
  },
  "u-p-r-44": {
    "paddingRight": "44rpx"
  },
  "u-margin-right-44": {
    "marginRight": "44rpx"
  },
  "u-padding-right-44": {
    "paddingRight": "44rpx"
  },
  "u-m-b-44": {
    "marginBottom": "44rpx"
  },
  "u-p-b-44": {
    "paddingBottom": "44rpx"
  },
  "u-margin-bottom-44": {
    "marginBottom": "44rpx"
  },
  "u-padding-bottom-44": {
    "paddingBottom": "44rpx"
  },
  "u-margin-45": {
    "marginTop": "45rpx",
    "marginRight": "45rpx",
    "marginBottom": "45rpx",
    "marginLeft": "45rpx"
  },
  "u-m-45": {
    "marginTop": "45rpx",
    "marginRight": "45rpx",
    "marginBottom": "45rpx",
    "marginLeft": "45rpx"
  },
  "u-padding-45": {
    "paddingTop": "45rpx",
    "paddingRight": "45rpx",
    "paddingBottom": "45rpx",
    "paddingLeft": "45rpx"
  },
  "u-p-45": {
    "paddingTop": "45rpx",
    "paddingRight": "45rpx",
    "paddingBottom": "45rpx",
    "paddingLeft": "45rpx"
  },
  "u-m-l-45": {
    "marginLeft": "45rpx"
  },
  "u-p-l-45": {
    "paddingLeft": "45rpx"
  },
  "u-margin-left-45": {
    "marginLeft": "45rpx"
  },
  "u-padding-left-45": {
    "paddingLeft": "45rpx"
  },
  "u-m-t-45": {
    "marginTop": "45rpx"
  },
  "u-p-t-45": {
    "paddingTop": "45rpx"
  },
  "u-margin-top-45": {
    "marginTop": "45rpx"
  },
  "u-padding-top-45": {
    "paddingTop": "45rpx"
  },
  "u-m-r-45": {
    "marginRight": "45rpx"
  },
  "u-p-r-45": {
    "paddingRight": "45rpx"
  },
  "u-margin-right-45": {
    "marginRight": "45rpx"
  },
  "u-padding-right-45": {
    "paddingRight": "45rpx"
  },
  "u-m-b-45": {
    "marginBottom": "45rpx"
  },
  "u-p-b-45": {
    "paddingBottom": "45rpx"
  },
  "u-margin-bottom-45": {
    "marginBottom": "45rpx"
  },
  "u-padding-bottom-45": {
    "paddingBottom": "45rpx"
  },
  "u-margin-46": {
    "marginTop": "46rpx",
    "marginRight": "46rpx",
    "marginBottom": "46rpx",
    "marginLeft": "46rpx"
  },
  "u-m-46": {
    "marginTop": "46rpx",
    "marginRight": "46rpx",
    "marginBottom": "46rpx",
    "marginLeft": "46rpx"
  },
  "u-padding-46": {
    "paddingTop": "46rpx",
    "paddingRight": "46rpx",
    "paddingBottom": "46rpx",
    "paddingLeft": "46rpx"
  },
  "u-p-46": {
    "paddingTop": "46rpx",
    "paddingRight": "46rpx",
    "paddingBottom": "46rpx",
    "paddingLeft": "46rpx"
  },
  "u-m-l-46": {
    "marginLeft": "46rpx"
  },
  "u-p-l-46": {
    "paddingLeft": "46rpx"
  },
  "u-margin-left-46": {
    "marginLeft": "46rpx"
  },
  "u-padding-left-46": {
    "paddingLeft": "46rpx"
  },
  "u-m-t-46": {
    "marginTop": "46rpx"
  },
  "u-p-t-46": {
    "paddingTop": "46rpx"
  },
  "u-margin-top-46": {
    "marginTop": "46rpx"
  },
  "u-padding-top-46": {
    "paddingTop": "46rpx"
  },
  "u-m-r-46": {
    "marginRight": "46rpx"
  },
  "u-p-r-46": {
    "paddingRight": "46rpx"
  },
  "u-margin-right-46": {
    "marginRight": "46rpx"
  },
  "u-padding-right-46": {
    "paddingRight": "46rpx"
  },
  "u-m-b-46": {
    "marginBottom": "46rpx"
  },
  "u-p-b-46": {
    "paddingBottom": "46rpx"
  },
  "u-margin-bottom-46": {
    "marginBottom": "46rpx"
  },
  "u-padding-bottom-46": {
    "paddingBottom": "46rpx"
  },
  "u-margin-48": {
    "marginTop": "48rpx",
    "marginRight": "48rpx",
    "marginBottom": "48rpx",
    "marginLeft": "48rpx"
  },
  "u-m-48": {
    "marginTop": "48rpx",
    "marginRight": "48rpx",
    "marginBottom": "48rpx",
    "marginLeft": "48rpx"
  },
  "u-padding-48": {
    "paddingTop": "48rpx",
    "paddingRight": "48rpx",
    "paddingBottom": "48rpx",
    "paddingLeft": "48rpx"
  },
  "u-p-48": {
    "paddingTop": "48rpx",
    "paddingRight": "48rpx",
    "paddingBottom": "48rpx",
    "paddingLeft": "48rpx"
  },
  "u-m-l-48": {
    "marginLeft": "48rpx"
  },
  "u-p-l-48": {
    "paddingLeft": "48rpx"
  },
  "u-margin-left-48": {
    "marginLeft": "48rpx"
  },
  "u-padding-left-48": {
    "paddingLeft": "48rpx"
  },
  "u-m-t-48": {
    "marginTop": "48rpx"
  },
  "u-p-t-48": {
    "paddingTop": "48rpx"
  },
  "u-margin-top-48": {
    "marginTop": "48rpx"
  },
  "u-padding-top-48": {
    "paddingTop": "48rpx"
  },
  "u-m-r-48": {
    "marginRight": "48rpx"
  },
  "u-p-r-48": {
    "paddingRight": "48rpx"
  },
  "u-margin-right-48": {
    "marginRight": "48rpx"
  },
  "u-padding-right-48": {
    "paddingRight": "48rpx"
  },
  "u-m-b-48": {
    "marginBottom": "48rpx"
  },
  "u-p-b-48": {
    "paddingBottom": "48rpx"
  },
  "u-margin-bottom-48": {
    "marginBottom": "48rpx"
  },
  "u-padding-bottom-48": {
    "paddingBottom": "48rpx"
  },
  "u-margin-50": {
    "marginTop": "50rpx",
    "marginRight": "50rpx",
    "marginBottom": "50rpx",
    "marginLeft": "50rpx"
  },
  "u-m-50": {
    "marginTop": "50rpx",
    "marginRight": "50rpx",
    "marginBottom": "50rpx",
    "marginLeft": "50rpx"
  },
  "u-padding-50": {
    "paddingTop": "50rpx",
    "paddingRight": "50rpx",
    "paddingBottom": "50rpx",
    "paddingLeft": "50rpx"
  },
  "u-p-50": {
    "paddingTop": "50rpx",
    "paddingRight": "50rpx",
    "paddingBottom": "50rpx",
    "paddingLeft": "50rpx"
  },
  "u-m-l-50": {
    "marginLeft": "50rpx"
  },
  "u-p-l-50": {
    "paddingLeft": "50rpx"
  },
  "u-margin-left-50": {
    "marginLeft": "50rpx"
  },
  "u-padding-left-50": {
    "paddingLeft": "50rpx"
  },
  "u-m-t-50": {
    "marginTop": "50rpx"
  },
  "u-p-t-50": {
    "paddingTop": "50rpx"
  },
  "u-margin-top-50": {
    "marginTop": "50rpx"
  },
  "u-padding-top-50": {
    "paddingTop": "50rpx"
  },
  "u-m-r-50": {
    "marginRight": "50rpx"
  },
  "u-p-r-50": {
    "paddingRight": "50rpx"
  },
  "u-margin-right-50": {
    "marginRight": "50rpx"
  },
  "u-padding-right-50": {
    "paddingRight": "50rpx"
  },
  "u-m-b-50": {
    "marginBottom": "50rpx"
  },
  "u-p-b-50": {
    "paddingBottom": "50rpx"
  },
  "u-margin-bottom-50": {
    "marginBottom": "50rpx"
  },
  "u-padding-bottom-50": {
    "paddingBottom": "50rpx"
  },
  "u-margin-52": {
    "marginTop": "52rpx",
    "marginRight": "52rpx",
    "marginBottom": "52rpx",
    "marginLeft": "52rpx"
  },
  "u-m-52": {
    "marginTop": "52rpx",
    "marginRight": "52rpx",
    "marginBottom": "52rpx",
    "marginLeft": "52rpx"
  },
  "u-padding-52": {
    "paddingTop": "52rpx",
    "paddingRight": "52rpx",
    "paddingBottom": "52rpx",
    "paddingLeft": "52rpx"
  },
  "u-p-52": {
    "paddingTop": "52rpx",
    "paddingRight": "52rpx",
    "paddingBottom": "52rpx",
    "paddingLeft": "52rpx"
  },
  "u-m-l-52": {
    "marginLeft": "52rpx"
  },
  "u-p-l-52": {
    "paddingLeft": "52rpx"
  },
  "u-margin-left-52": {
    "marginLeft": "52rpx"
  },
  "u-padding-left-52": {
    "paddingLeft": "52rpx"
  },
  "u-m-t-52": {
    "marginTop": "52rpx"
  },
  "u-p-t-52": {
    "paddingTop": "52rpx"
  },
  "u-margin-top-52": {
    "marginTop": "52rpx"
  },
  "u-padding-top-52": {
    "paddingTop": "52rpx"
  },
  "u-m-r-52": {
    "marginRight": "52rpx"
  },
  "u-p-r-52": {
    "paddingRight": "52rpx"
  },
  "u-margin-right-52": {
    "marginRight": "52rpx"
  },
  "u-padding-right-52": {
    "paddingRight": "52rpx"
  },
  "u-m-b-52": {
    "marginBottom": "52rpx"
  },
  "u-p-b-52": {
    "paddingBottom": "52rpx"
  },
  "u-margin-bottom-52": {
    "marginBottom": "52rpx"
  },
  "u-padding-bottom-52": {
    "paddingBottom": "52rpx"
  },
  "u-margin-54": {
    "marginTop": "54rpx",
    "marginRight": "54rpx",
    "marginBottom": "54rpx",
    "marginLeft": "54rpx"
  },
  "u-m-54": {
    "marginTop": "54rpx",
    "marginRight": "54rpx",
    "marginBottom": "54rpx",
    "marginLeft": "54rpx"
  },
  "u-padding-54": {
    "paddingTop": "54rpx",
    "paddingRight": "54rpx",
    "paddingBottom": "54rpx",
    "paddingLeft": "54rpx"
  },
  "u-p-54": {
    "paddingTop": "54rpx",
    "paddingRight": "54rpx",
    "paddingBottom": "54rpx",
    "paddingLeft": "54rpx"
  },
  "u-m-l-54": {
    "marginLeft": "54rpx"
  },
  "u-p-l-54": {
    "paddingLeft": "54rpx"
  },
  "u-margin-left-54": {
    "marginLeft": "54rpx"
  },
  "u-padding-left-54": {
    "paddingLeft": "54rpx"
  },
  "u-m-t-54": {
    "marginTop": "54rpx"
  },
  "u-p-t-54": {
    "paddingTop": "54rpx"
  },
  "u-margin-top-54": {
    "marginTop": "54rpx"
  },
  "u-padding-top-54": {
    "paddingTop": "54rpx"
  },
  "u-m-r-54": {
    "marginRight": "54rpx"
  },
  "u-p-r-54": {
    "paddingRight": "54rpx"
  },
  "u-margin-right-54": {
    "marginRight": "54rpx"
  },
  "u-padding-right-54": {
    "paddingRight": "54rpx"
  },
  "u-m-b-54": {
    "marginBottom": "54rpx"
  },
  "u-p-b-54": {
    "paddingBottom": "54rpx"
  },
  "u-margin-bottom-54": {
    "marginBottom": "54rpx"
  },
  "u-padding-bottom-54": {
    "paddingBottom": "54rpx"
  },
  "u-margin-55": {
    "marginTop": "55rpx",
    "marginRight": "55rpx",
    "marginBottom": "55rpx",
    "marginLeft": "55rpx"
  },
  "u-m-55": {
    "marginTop": "55rpx",
    "marginRight": "55rpx",
    "marginBottom": "55rpx",
    "marginLeft": "55rpx"
  },
  "u-padding-55": {
    "paddingTop": "55rpx",
    "paddingRight": "55rpx",
    "paddingBottom": "55rpx",
    "paddingLeft": "55rpx"
  },
  "u-p-55": {
    "paddingTop": "55rpx",
    "paddingRight": "55rpx",
    "paddingBottom": "55rpx",
    "paddingLeft": "55rpx"
  },
  "u-m-l-55": {
    "marginLeft": "55rpx"
  },
  "u-p-l-55": {
    "paddingLeft": "55rpx"
  },
  "u-margin-left-55": {
    "marginLeft": "55rpx"
  },
  "u-padding-left-55": {
    "paddingLeft": "55rpx"
  },
  "u-m-t-55": {
    "marginTop": "55rpx"
  },
  "u-p-t-55": {
    "paddingTop": "55rpx"
  },
  "u-margin-top-55": {
    "marginTop": "55rpx"
  },
  "u-padding-top-55": {
    "paddingTop": "55rpx"
  },
  "u-m-r-55": {
    "marginRight": "55rpx"
  },
  "u-p-r-55": {
    "paddingRight": "55rpx"
  },
  "u-margin-right-55": {
    "marginRight": "55rpx"
  },
  "u-padding-right-55": {
    "paddingRight": "55rpx"
  },
  "u-m-b-55": {
    "marginBottom": "55rpx"
  },
  "u-p-b-55": {
    "paddingBottom": "55rpx"
  },
  "u-margin-bottom-55": {
    "marginBottom": "55rpx"
  },
  "u-padding-bottom-55": {
    "paddingBottom": "55rpx"
  },
  "u-margin-56": {
    "marginTop": "56rpx",
    "marginRight": "56rpx",
    "marginBottom": "56rpx",
    "marginLeft": "56rpx"
  },
  "u-m-56": {
    "marginTop": "56rpx",
    "marginRight": "56rpx",
    "marginBottom": "56rpx",
    "marginLeft": "56rpx"
  },
  "u-padding-56": {
    "paddingTop": "56rpx",
    "paddingRight": "56rpx",
    "paddingBottom": "56rpx",
    "paddingLeft": "56rpx"
  },
  "u-p-56": {
    "paddingTop": "56rpx",
    "paddingRight": "56rpx",
    "paddingBottom": "56rpx",
    "paddingLeft": "56rpx"
  },
  "u-m-l-56": {
    "marginLeft": "56rpx"
  },
  "u-p-l-56": {
    "paddingLeft": "56rpx"
  },
  "u-margin-left-56": {
    "marginLeft": "56rpx"
  },
  "u-padding-left-56": {
    "paddingLeft": "56rpx"
  },
  "u-m-t-56": {
    "marginTop": "56rpx"
  },
  "u-p-t-56": {
    "paddingTop": "56rpx"
  },
  "u-margin-top-56": {
    "marginTop": "56rpx"
  },
  "u-padding-top-56": {
    "paddingTop": "56rpx"
  },
  "u-m-r-56": {
    "marginRight": "56rpx"
  },
  "u-p-r-56": {
    "paddingRight": "56rpx"
  },
  "u-margin-right-56": {
    "marginRight": "56rpx"
  },
  "u-padding-right-56": {
    "paddingRight": "56rpx"
  },
  "u-m-b-56": {
    "marginBottom": "56rpx"
  },
  "u-p-b-56": {
    "paddingBottom": "56rpx"
  },
  "u-margin-bottom-56": {
    "marginBottom": "56rpx"
  },
  "u-padding-bottom-56": {
    "paddingBottom": "56rpx"
  },
  "u-margin-58": {
    "marginTop": "58rpx",
    "marginRight": "58rpx",
    "marginBottom": "58rpx",
    "marginLeft": "58rpx"
  },
  "u-m-58": {
    "marginTop": "58rpx",
    "marginRight": "58rpx",
    "marginBottom": "58rpx",
    "marginLeft": "58rpx"
  },
  "u-padding-58": {
    "paddingTop": "58rpx",
    "paddingRight": "58rpx",
    "paddingBottom": "58rpx",
    "paddingLeft": "58rpx"
  },
  "u-p-58": {
    "paddingTop": "58rpx",
    "paddingRight": "58rpx",
    "paddingBottom": "58rpx",
    "paddingLeft": "58rpx"
  },
  "u-m-l-58": {
    "marginLeft": "58rpx"
  },
  "u-p-l-58": {
    "paddingLeft": "58rpx"
  },
  "u-margin-left-58": {
    "marginLeft": "58rpx"
  },
  "u-padding-left-58": {
    "paddingLeft": "58rpx"
  },
  "u-m-t-58": {
    "marginTop": "58rpx"
  },
  "u-p-t-58": {
    "paddingTop": "58rpx"
  },
  "u-margin-top-58": {
    "marginTop": "58rpx"
  },
  "u-padding-top-58": {
    "paddingTop": "58rpx"
  },
  "u-m-r-58": {
    "marginRight": "58rpx"
  },
  "u-p-r-58": {
    "paddingRight": "58rpx"
  },
  "u-margin-right-58": {
    "marginRight": "58rpx"
  },
  "u-padding-right-58": {
    "paddingRight": "58rpx"
  },
  "u-m-b-58": {
    "marginBottom": "58rpx"
  },
  "u-p-b-58": {
    "paddingBottom": "58rpx"
  },
  "u-margin-bottom-58": {
    "marginBottom": "58rpx"
  },
  "u-padding-bottom-58": {
    "paddingBottom": "58rpx"
  },
  "u-margin-60": {
    "marginTop": "60rpx",
    "marginRight": "60rpx",
    "marginBottom": "60rpx",
    "marginLeft": "60rpx"
  },
  "u-m-60": {
    "marginTop": "60rpx",
    "marginRight": "60rpx",
    "marginBottom": "60rpx",
    "marginLeft": "60rpx"
  },
  "u-padding-60": {
    "paddingTop": "60rpx",
    "paddingRight": "60rpx",
    "paddingBottom": "60rpx",
    "paddingLeft": "60rpx"
  },
  "u-p-60": {
    "paddingTop": "60rpx",
    "paddingRight": "60rpx",
    "paddingBottom": "60rpx",
    "paddingLeft": "60rpx"
  },
  "u-m-l-60": {
    "marginLeft": "60rpx"
  },
  "u-p-l-60": {
    "paddingLeft": "60rpx"
  },
  "u-margin-left-60": {
    "marginLeft": "60rpx"
  },
  "u-padding-left-60": {
    "paddingLeft": "60rpx"
  },
  "u-m-t-60": {
    "marginTop": "60rpx"
  },
  "u-p-t-60": {
    "paddingTop": "60rpx"
  },
  "u-margin-top-60": {
    "marginTop": "60rpx"
  },
  "u-padding-top-60": {
    "paddingTop": "60rpx"
  },
  "u-m-r-60": {
    "marginRight": "60rpx"
  },
  "u-p-r-60": {
    "paddingRight": "60rpx"
  },
  "u-margin-right-60": {
    "marginRight": "60rpx"
  },
  "u-padding-right-60": {
    "paddingRight": "60rpx"
  },
  "u-m-b-60": {
    "marginBottom": "60rpx"
  },
  "u-p-b-60": {
    "paddingBottom": "60rpx"
  },
  "u-margin-bottom-60": {
    "marginBottom": "60rpx"
  },
  "u-padding-bottom-60": {
    "paddingBottom": "60rpx"
  },
  "u-margin-62": {
    "marginTop": "62rpx",
    "marginRight": "62rpx",
    "marginBottom": "62rpx",
    "marginLeft": "62rpx"
  },
  "u-m-62": {
    "marginTop": "62rpx",
    "marginRight": "62rpx",
    "marginBottom": "62rpx",
    "marginLeft": "62rpx"
  },
  "u-padding-62": {
    "paddingTop": "62rpx",
    "paddingRight": "62rpx",
    "paddingBottom": "62rpx",
    "paddingLeft": "62rpx"
  },
  "u-p-62": {
    "paddingTop": "62rpx",
    "paddingRight": "62rpx",
    "paddingBottom": "62rpx",
    "paddingLeft": "62rpx"
  },
  "u-m-l-62": {
    "marginLeft": "62rpx"
  },
  "u-p-l-62": {
    "paddingLeft": "62rpx"
  },
  "u-margin-left-62": {
    "marginLeft": "62rpx"
  },
  "u-padding-left-62": {
    "paddingLeft": "62rpx"
  },
  "u-m-t-62": {
    "marginTop": "62rpx"
  },
  "u-p-t-62": {
    "paddingTop": "62rpx"
  },
  "u-margin-top-62": {
    "marginTop": "62rpx"
  },
  "u-padding-top-62": {
    "paddingTop": "62rpx"
  },
  "u-m-r-62": {
    "marginRight": "62rpx"
  },
  "u-p-r-62": {
    "paddingRight": "62rpx"
  },
  "u-margin-right-62": {
    "marginRight": "62rpx"
  },
  "u-padding-right-62": {
    "paddingRight": "62rpx"
  },
  "u-m-b-62": {
    "marginBottom": "62rpx"
  },
  "u-p-b-62": {
    "paddingBottom": "62rpx"
  },
  "u-margin-bottom-62": {
    "marginBottom": "62rpx"
  },
  "u-padding-bottom-62": {
    "paddingBottom": "62rpx"
  },
  "u-margin-64": {
    "marginTop": "64rpx",
    "marginRight": "64rpx",
    "marginBottom": "64rpx",
    "marginLeft": "64rpx"
  },
  "u-m-64": {
    "marginTop": "64rpx",
    "marginRight": "64rpx",
    "marginBottom": "64rpx",
    "marginLeft": "64rpx"
  },
  "u-padding-64": {
    "paddingTop": "64rpx",
    "paddingRight": "64rpx",
    "paddingBottom": "64rpx",
    "paddingLeft": "64rpx"
  },
  "u-p-64": {
    "paddingTop": "64rpx",
    "paddingRight": "64rpx",
    "paddingBottom": "64rpx",
    "paddingLeft": "64rpx"
  },
  "u-m-l-64": {
    "marginLeft": "64rpx"
  },
  "u-p-l-64": {
    "paddingLeft": "64rpx"
  },
  "u-margin-left-64": {
    "marginLeft": "64rpx"
  },
  "u-padding-left-64": {
    "paddingLeft": "64rpx"
  },
  "u-m-t-64": {
    "marginTop": "64rpx"
  },
  "u-p-t-64": {
    "paddingTop": "64rpx"
  },
  "u-margin-top-64": {
    "marginTop": "64rpx"
  },
  "u-padding-top-64": {
    "paddingTop": "64rpx"
  },
  "u-m-r-64": {
    "marginRight": "64rpx"
  },
  "u-p-r-64": {
    "paddingRight": "64rpx"
  },
  "u-margin-right-64": {
    "marginRight": "64rpx"
  },
  "u-padding-right-64": {
    "paddingRight": "64rpx"
  },
  "u-m-b-64": {
    "marginBottom": "64rpx"
  },
  "u-p-b-64": {
    "paddingBottom": "64rpx"
  },
  "u-margin-bottom-64": {
    "marginBottom": "64rpx"
  },
  "u-padding-bottom-64": {
    "paddingBottom": "64rpx"
  },
  "u-margin-65": {
    "marginTop": "65rpx",
    "marginRight": "65rpx",
    "marginBottom": "65rpx",
    "marginLeft": "65rpx"
  },
  "u-m-65": {
    "marginTop": "65rpx",
    "marginRight": "65rpx",
    "marginBottom": "65rpx",
    "marginLeft": "65rpx"
  },
  "u-padding-65": {
    "paddingTop": "65rpx",
    "paddingRight": "65rpx",
    "paddingBottom": "65rpx",
    "paddingLeft": "65rpx"
  },
  "u-p-65": {
    "paddingTop": "65rpx",
    "paddingRight": "65rpx",
    "paddingBottom": "65rpx",
    "paddingLeft": "65rpx"
  },
  "u-m-l-65": {
    "marginLeft": "65rpx"
  },
  "u-p-l-65": {
    "paddingLeft": "65rpx"
  },
  "u-margin-left-65": {
    "marginLeft": "65rpx"
  },
  "u-padding-left-65": {
    "paddingLeft": "65rpx"
  },
  "u-m-t-65": {
    "marginTop": "65rpx"
  },
  "u-p-t-65": {
    "paddingTop": "65rpx"
  },
  "u-margin-top-65": {
    "marginTop": "65rpx"
  },
  "u-padding-top-65": {
    "paddingTop": "65rpx"
  },
  "u-m-r-65": {
    "marginRight": "65rpx"
  },
  "u-p-r-65": {
    "paddingRight": "65rpx"
  },
  "u-margin-right-65": {
    "marginRight": "65rpx"
  },
  "u-padding-right-65": {
    "paddingRight": "65rpx"
  },
  "u-m-b-65": {
    "marginBottom": "65rpx"
  },
  "u-p-b-65": {
    "paddingBottom": "65rpx"
  },
  "u-margin-bottom-65": {
    "marginBottom": "65rpx"
  },
  "u-padding-bottom-65": {
    "paddingBottom": "65rpx"
  },
  "u-margin-66": {
    "marginTop": "66rpx",
    "marginRight": "66rpx",
    "marginBottom": "66rpx",
    "marginLeft": "66rpx"
  },
  "u-m-66": {
    "marginTop": "66rpx",
    "marginRight": "66rpx",
    "marginBottom": "66rpx",
    "marginLeft": "66rpx"
  },
  "u-padding-66": {
    "paddingTop": "66rpx",
    "paddingRight": "66rpx",
    "paddingBottom": "66rpx",
    "paddingLeft": "66rpx"
  },
  "u-p-66": {
    "paddingTop": "66rpx",
    "paddingRight": "66rpx",
    "paddingBottom": "66rpx",
    "paddingLeft": "66rpx"
  },
  "u-m-l-66": {
    "marginLeft": "66rpx"
  },
  "u-p-l-66": {
    "paddingLeft": "66rpx"
  },
  "u-margin-left-66": {
    "marginLeft": "66rpx"
  },
  "u-padding-left-66": {
    "paddingLeft": "66rpx"
  },
  "u-m-t-66": {
    "marginTop": "66rpx"
  },
  "u-p-t-66": {
    "paddingTop": "66rpx"
  },
  "u-margin-top-66": {
    "marginTop": "66rpx"
  },
  "u-padding-top-66": {
    "paddingTop": "66rpx"
  },
  "u-m-r-66": {
    "marginRight": "66rpx"
  },
  "u-p-r-66": {
    "paddingRight": "66rpx"
  },
  "u-margin-right-66": {
    "marginRight": "66rpx"
  },
  "u-padding-right-66": {
    "paddingRight": "66rpx"
  },
  "u-m-b-66": {
    "marginBottom": "66rpx"
  },
  "u-p-b-66": {
    "paddingBottom": "66rpx"
  },
  "u-margin-bottom-66": {
    "marginBottom": "66rpx"
  },
  "u-padding-bottom-66": {
    "paddingBottom": "66rpx"
  },
  "u-margin-68": {
    "marginTop": "68rpx",
    "marginRight": "68rpx",
    "marginBottom": "68rpx",
    "marginLeft": "68rpx"
  },
  "u-m-68": {
    "marginTop": "68rpx",
    "marginRight": "68rpx",
    "marginBottom": "68rpx",
    "marginLeft": "68rpx"
  },
  "u-padding-68": {
    "paddingTop": "68rpx",
    "paddingRight": "68rpx",
    "paddingBottom": "68rpx",
    "paddingLeft": "68rpx"
  },
  "u-p-68": {
    "paddingTop": "68rpx",
    "paddingRight": "68rpx",
    "paddingBottom": "68rpx",
    "paddingLeft": "68rpx"
  },
  "u-m-l-68": {
    "marginLeft": "68rpx"
  },
  "u-p-l-68": {
    "paddingLeft": "68rpx"
  },
  "u-margin-left-68": {
    "marginLeft": "68rpx"
  },
  "u-padding-left-68": {
    "paddingLeft": "68rpx"
  },
  "u-m-t-68": {
    "marginTop": "68rpx"
  },
  "u-p-t-68": {
    "paddingTop": "68rpx"
  },
  "u-margin-top-68": {
    "marginTop": "68rpx"
  },
  "u-padding-top-68": {
    "paddingTop": "68rpx"
  },
  "u-m-r-68": {
    "marginRight": "68rpx"
  },
  "u-p-r-68": {
    "paddingRight": "68rpx"
  },
  "u-margin-right-68": {
    "marginRight": "68rpx"
  },
  "u-padding-right-68": {
    "paddingRight": "68rpx"
  },
  "u-m-b-68": {
    "marginBottom": "68rpx"
  },
  "u-p-b-68": {
    "paddingBottom": "68rpx"
  },
  "u-margin-bottom-68": {
    "marginBottom": "68rpx"
  },
  "u-padding-bottom-68": {
    "paddingBottom": "68rpx"
  },
  "u-margin-70": {
    "marginTop": "70rpx",
    "marginRight": "70rpx",
    "marginBottom": "70rpx",
    "marginLeft": "70rpx"
  },
  "u-m-70": {
    "marginTop": "70rpx",
    "marginRight": "70rpx",
    "marginBottom": "70rpx",
    "marginLeft": "70rpx"
  },
  "u-padding-70": {
    "paddingTop": "70rpx",
    "paddingRight": "70rpx",
    "paddingBottom": "70rpx",
    "paddingLeft": "70rpx"
  },
  "u-p-70": {
    "paddingTop": "70rpx",
    "paddingRight": "70rpx",
    "paddingBottom": "70rpx",
    "paddingLeft": "70rpx"
  },
  "u-m-l-70": {
    "marginLeft": "70rpx"
  },
  "u-p-l-70": {
    "paddingLeft": "70rpx"
  },
  "u-margin-left-70": {
    "marginLeft": "70rpx"
  },
  "u-padding-left-70": {
    "paddingLeft": "70rpx"
  },
  "u-m-t-70": {
    "marginTop": "70rpx"
  },
  "u-p-t-70": {
    "paddingTop": "70rpx"
  },
  "u-margin-top-70": {
    "marginTop": "70rpx"
  },
  "u-padding-top-70": {
    "paddingTop": "70rpx"
  },
  "u-m-r-70": {
    "marginRight": "70rpx"
  },
  "u-p-r-70": {
    "paddingRight": "70rpx"
  },
  "u-margin-right-70": {
    "marginRight": "70rpx"
  },
  "u-padding-right-70": {
    "paddingRight": "70rpx"
  },
  "u-m-b-70": {
    "marginBottom": "70rpx"
  },
  "u-p-b-70": {
    "paddingBottom": "70rpx"
  },
  "u-margin-bottom-70": {
    "marginBottom": "70rpx"
  },
  "u-padding-bottom-70": {
    "paddingBottom": "70rpx"
  },
  "u-margin-72": {
    "marginTop": "72rpx",
    "marginRight": "72rpx",
    "marginBottom": "72rpx",
    "marginLeft": "72rpx"
  },
  "u-m-72": {
    "marginTop": "72rpx",
    "marginRight": "72rpx",
    "marginBottom": "72rpx",
    "marginLeft": "72rpx"
  },
  "u-padding-72": {
    "paddingTop": "72rpx",
    "paddingRight": "72rpx",
    "paddingBottom": "72rpx",
    "paddingLeft": "72rpx"
  },
  "u-p-72": {
    "paddingTop": "72rpx",
    "paddingRight": "72rpx",
    "paddingBottom": "72rpx",
    "paddingLeft": "72rpx"
  },
  "u-m-l-72": {
    "marginLeft": "72rpx"
  },
  "u-p-l-72": {
    "paddingLeft": "72rpx"
  },
  "u-margin-left-72": {
    "marginLeft": "72rpx"
  },
  "u-padding-left-72": {
    "paddingLeft": "72rpx"
  },
  "u-m-t-72": {
    "marginTop": "72rpx"
  },
  "u-p-t-72": {
    "paddingTop": "72rpx"
  },
  "u-margin-top-72": {
    "marginTop": "72rpx"
  },
  "u-padding-top-72": {
    "paddingTop": "72rpx"
  },
  "u-m-r-72": {
    "marginRight": "72rpx"
  },
  "u-p-r-72": {
    "paddingRight": "72rpx"
  },
  "u-margin-right-72": {
    "marginRight": "72rpx"
  },
  "u-padding-right-72": {
    "paddingRight": "72rpx"
  },
  "u-m-b-72": {
    "marginBottom": "72rpx"
  },
  "u-p-b-72": {
    "paddingBottom": "72rpx"
  },
  "u-margin-bottom-72": {
    "marginBottom": "72rpx"
  },
  "u-padding-bottom-72": {
    "paddingBottom": "72rpx"
  },
  "u-margin-74": {
    "marginTop": "74rpx",
    "marginRight": "74rpx",
    "marginBottom": "74rpx",
    "marginLeft": "74rpx"
  },
  "u-m-74": {
    "marginTop": "74rpx",
    "marginRight": "74rpx",
    "marginBottom": "74rpx",
    "marginLeft": "74rpx"
  },
  "u-padding-74": {
    "paddingTop": "74rpx",
    "paddingRight": "74rpx",
    "paddingBottom": "74rpx",
    "paddingLeft": "74rpx"
  },
  "u-p-74": {
    "paddingTop": "74rpx",
    "paddingRight": "74rpx",
    "paddingBottom": "74rpx",
    "paddingLeft": "74rpx"
  },
  "u-m-l-74": {
    "marginLeft": "74rpx"
  },
  "u-p-l-74": {
    "paddingLeft": "74rpx"
  },
  "u-margin-left-74": {
    "marginLeft": "74rpx"
  },
  "u-padding-left-74": {
    "paddingLeft": "74rpx"
  },
  "u-m-t-74": {
    "marginTop": "74rpx"
  },
  "u-p-t-74": {
    "paddingTop": "74rpx"
  },
  "u-margin-top-74": {
    "marginTop": "74rpx"
  },
  "u-padding-top-74": {
    "paddingTop": "74rpx"
  },
  "u-m-r-74": {
    "marginRight": "74rpx"
  },
  "u-p-r-74": {
    "paddingRight": "74rpx"
  },
  "u-margin-right-74": {
    "marginRight": "74rpx"
  },
  "u-padding-right-74": {
    "paddingRight": "74rpx"
  },
  "u-m-b-74": {
    "marginBottom": "74rpx"
  },
  "u-p-b-74": {
    "paddingBottom": "74rpx"
  },
  "u-margin-bottom-74": {
    "marginBottom": "74rpx"
  },
  "u-padding-bottom-74": {
    "paddingBottom": "74rpx"
  },
  "u-margin-75": {
    "marginTop": "75rpx",
    "marginRight": "75rpx",
    "marginBottom": "75rpx",
    "marginLeft": "75rpx"
  },
  "u-m-75": {
    "marginTop": "75rpx",
    "marginRight": "75rpx",
    "marginBottom": "75rpx",
    "marginLeft": "75rpx"
  },
  "u-padding-75": {
    "paddingTop": "75rpx",
    "paddingRight": "75rpx",
    "paddingBottom": "75rpx",
    "paddingLeft": "75rpx"
  },
  "u-p-75": {
    "paddingTop": "75rpx",
    "paddingRight": "75rpx",
    "paddingBottom": "75rpx",
    "paddingLeft": "75rpx"
  },
  "u-m-l-75": {
    "marginLeft": "75rpx"
  },
  "u-p-l-75": {
    "paddingLeft": "75rpx"
  },
  "u-margin-left-75": {
    "marginLeft": "75rpx"
  },
  "u-padding-left-75": {
    "paddingLeft": "75rpx"
  },
  "u-m-t-75": {
    "marginTop": "75rpx"
  },
  "u-p-t-75": {
    "paddingTop": "75rpx"
  },
  "u-margin-top-75": {
    "marginTop": "75rpx"
  },
  "u-padding-top-75": {
    "paddingTop": "75rpx"
  },
  "u-m-r-75": {
    "marginRight": "75rpx"
  },
  "u-p-r-75": {
    "paddingRight": "75rpx"
  },
  "u-margin-right-75": {
    "marginRight": "75rpx"
  },
  "u-padding-right-75": {
    "paddingRight": "75rpx"
  },
  "u-m-b-75": {
    "marginBottom": "75rpx"
  },
  "u-p-b-75": {
    "paddingBottom": "75rpx"
  },
  "u-margin-bottom-75": {
    "marginBottom": "75rpx"
  },
  "u-padding-bottom-75": {
    "paddingBottom": "75rpx"
  },
  "u-margin-76": {
    "marginTop": "76rpx",
    "marginRight": "76rpx",
    "marginBottom": "76rpx",
    "marginLeft": "76rpx"
  },
  "u-m-76": {
    "marginTop": "76rpx",
    "marginRight": "76rpx",
    "marginBottom": "76rpx",
    "marginLeft": "76rpx"
  },
  "u-padding-76": {
    "paddingTop": "76rpx",
    "paddingRight": "76rpx",
    "paddingBottom": "76rpx",
    "paddingLeft": "76rpx"
  },
  "u-p-76": {
    "paddingTop": "76rpx",
    "paddingRight": "76rpx",
    "paddingBottom": "76rpx",
    "paddingLeft": "76rpx"
  },
  "u-m-l-76": {
    "marginLeft": "76rpx"
  },
  "u-p-l-76": {
    "paddingLeft": "76rpx"
  },
  "u-margin-left-76": {
    "marginLeft": "76rpx"
  },
  "u-padding-left-76": {
    "paddingLeft": "76rpx"
  },
  "u-m-t-76": {
    "marginTop": "76rpx"
  },
  "u-p-t-76": {
    "paddingTop": "76rpx"
  },
  "u-margin-top-76": {
    "marginTop": "76rpx"
  },
  "u-padding-top-76": {
    "paddingTop": "76rpx"
  },
  "u-m-r-76": {
    "marginRight": "76rpx"
  },
  "u-p-r-76": {
    "paddingRight": "76rpx"
  },
  "u-margin-right-76": {
    "marginRight": "76rpx"
  },
  "u-padding-right-76": {
    "paddingRight": "76rpx"
  },
  "u-m-b-76": {
    "marginBottom": "76rpx"
  },
  "u-p-b-76": {
    "paddingBottom": "76rpx"
  },
  "u-margin-bottom-76": {
    "marginBottom": "76rpx"
  },
  "u-padding-bottom-76": {
    "paddingBottom": "76rpx"
  },
  "u-margin-78": {
    "marginTop": "78rpx",
    "marginRight": "78rpx",
    "marginBottom": "78rpx",
    "marginLeft": "78rpx"
  },
  "u-m-78": {
    "marginTop": "78rpx",
    "marginRight": "78rpx",
    "marginBottom": "78rpx",
    "marginLeft": "78rpx"
  },
  "u-padding-78": {
    "paddingTop": "78rpx",
    "paddingRight": "78rpx",
    "paddingBottom": "78rpx",
    "paddingLeft": "78rpx"
  },
  "u-p-78": {
    "paddingTop": "78rpx",
    "paddingRight": "78rpx",
    "paddingBottom": "78rpx",
    "paddingLeft": "78rpx"
  },
  "u-m-l-78": {
    "marginLeft": "78rpx"
  },
  "u-p-l-78": {
    "paddingLeft": "78rpx"
  },
  "u-margin-left-78": {
    "marginLeft": "78rpx"
  },
  "u-padding-left-78": {
    "paddingLeft": "78rpx"
  },
  "u-m-t-78": {
    "marginTop": "78rpx"
  },
  "u-p-t-78": {
    "paddingTop": "78rpx"
  },
  "u-margin-top-78": {
    "marginTop": "78rpx"
  },
  "u-padding-top-78": {
    "paddingTop": "78rpx"
  },
  "u-m-r-78": {
    "marginRight": "78rpx"
  },
  "u-p-r-78": {
    "paddingRight": "78rpx"
  },
  "u-margin-right-78": {
    "marginRight": "78rpx"
  },
  "u-padding-right-78": {
    "paddingRight": "78rpx"
  },
  "u-m-b-78": {
    "marginBottom": "78rpx"
  },
  "u-p-b-78": {
    "paddingBottom": "78rpx"
  },
  "u-margin-bottom-78": {
    "marginBottom": "78rpx"
  },
  "u-padding-bottom-78": {
    "paddingBottom": "78rpx"
  },
  "u-margin-80": {
    "marginTop": "80rpx",
    "marginRight": "80rpx",
    "marginBottom": "80rpx",
    "marginLeft": "80rpx"
  },
  "u-m-80": {
    "marginTop": "80rpx",
    "marginRight": "80rpx",
    "marginBottom": "80rpx",
    "marginLeft": "80rpx"
  },
  "u-padding-80": {
    "paddingTop": "80rpx",
    "paddingRight": "80rpx",
    "paddingBottom": "80rpx",
    "paddingLeft": "80rpx"
  },
  "u-p-80": {
    "paddingTop": "80rpx",
    "paddingRight": "80rpx",
    "paddingBottom": "80rpx",
    "paddingLeft": "80rpx"
  },
  "u-m-l-80": {
    "marginLeft": "80rpx"
  },
  "u-p-l-80": {
    "paddingLeft": "80rpx"
  },
  "u-margin-left-80": {
    "marginLeft": "80rpx"
  },
  "u-padding-left-80": {
    "paddingLeft": "80rpx"
  },
  "u-m-t-80": {
    "marginTop": "80rpx"
  },
  "u-p-t-80": {
    "paddingTop": "80rpx"
  },
  "u-margin-top-80": {
    "marginTop": "80rpx"
  },
  "u-padding-top-80": {
    "paddingTop": "80rpx"
  },
  "u-m-r-80": {
    "marginRight": "80rpx"
  },
  "u-p-r-80": {
    "paddingRight": "80rpx"
  },
  "u-margin-right-80": {
    "marginRight": "80rpx"
  },
  "u-padding-right-80": {
    "paddingRight": "80rpx"
  },
  "u-m-b-80": {
    "marginBottom": "80rpx"
  },
  "u-p-b-80": {
    "paddingBottom": "80rpx"
  },
  "u-margin-bottom-80": {
    "marginBottom": "80rpx"
  },
  "u-padding-bottom-80": {
    "paddingBottom": "80rpx"
  },
  "u-reset-nvue": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "u-type-primary-light": {
    "color": "#ecf5ff"
  },
  "u-type-warning-light": {
    "color": "#fdf6ec"
  },
  "u-type-success-light": {
    "color": "#dbf1e1"
  },
  "u-type-error-light": {
    "color": "#fef0f0"
  },
  "u-type-info-light": {
    "color": "#f4f4f5"
  },
  "u-type-primary-light-bg": {
    "backgroundColor": "#ecf5ff"
  },
  "u-type-warning-light-bg": {
    "backgroundColor": "#fdf6ec"
  },
  "u-type-success-light-bg": {
    "backgroundColor": "#dbf1e1"
  },
  "u-type-error-light-bg": {
    "backgroundColor": "#fef0f0"
  },
  "u-type-info-light-bg": {
    "backgroundColor": "#f4f4f5"
  },
  "u-type-primary-dark": {
    "color": "#2b85e4"
  },
  "u-type-warning-dark": {
    "color": "#f29100"
  },
  "u-type-success-dark": {
    "color": "#18b566"
  },
  "u-type-error-dark": {
    "color": "#dd6161"
  },
  "u-type-info-dark": {
    "color": "#82848a"
  },
  "u-type-primary-dark-bg": {
    "backgroundColor": "#2b85e4"
  },
  "u-type-warning-dark-bg": {
    "backgroundColor": "#f29100"
  },
  "u-type-success-dark-bg": {
    "backgroundColor": "#18b566"
  },
  "u-type-error-dark-bg": {
    "backgroundColor": "#dd6161"
  },
  "u-type-info-dark-bg": {
    "backgroundColor": "#82848a"
  },
  "u-type-primary-disabled": {
    "color": "#a0cfff"
  },
  "u-type-warning-disabled": {
    "color": "#fcbd71"
  },
  "u-type-success-disabled": {
    "color": "#71d5a1"
  },
  "u-type-error-disabled": {
    "color": "#fab6b6"
  },
  "u-type-info-disabled": {
    "color": "#c8c9cc"
  },
  "u-type-primary": {
    "color": "#2979ff"
  },
  "u-type-warning": {
    "color": "#ff9900"
  },
  "u-type-success": {
    "color": "#19be6b"
  },
  "u-type-error": {
    "color": "#fa3534"
  },
  "u-type-info": {
    "color": "#909399"
  },
  "u-type-primary-bg": {
    "backgroundColor": "#2979ff"
  },
  "u-type-warning-bg": {
    "backgroundColor": "#ff9900"
  },
  "u-type-success-bg": {
    "backgroundColor": "#19be6b"
  },
  "u-type-error-bg": {
    "backgroundColor": "#fa3534"
  },
  "u-type-info-bg": {
    "backgroundColor": "#909399"
  },
  "u-main-color": {
    "color": "#303133"
  },
  "u-content-color": {
    "color": "#606266"
  },
  "u-tips-color": {
    "color": "#909399"
  },
  "u-light-color": {
    "color": "#c0c4cc"
  },
  "nvue": {
    "fontSize": "24rpx"
  }
}

/***/ }),
/* 4 */
/*!************************************************************************************************************!*\
  !*** /Users/caijizhou/Documents/HBuilderProjects/yoki-live/pages/create-live/create-live.nvue?mpType=page ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./create-live.nvue?vue&type=template&id=3de7a928&mpType=page */ 5);\n/* harmony import */ var _create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./create-live.nvue?vue&type=script&lang=js&mpType=page */ 7);\n/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));\n/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js */ 10);\n\nvar renderjs\n\n\nfunction injectStyles (context) {\n  \n  if(!this.options.style){\n          this.options.style = {}\n      }\n      if(Vue.prototype.__merge_style && Vue.prototype.__$appStyle__){\n        Vue.prototype.__merge_style(Vue.prototype.__$appStyle__, this.options.style)\n      }\n      \n}\n\n/* normalize component */\n\nvar component = Object(_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(\n  _create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  _create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  null,\n  \"b4b356ae\",\n  false,\n  _create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__[\"components\"],\n  renderjs\n)\n\ninjectStyles.call(component)\ncomponent.options.__file = \"pages/create-live/create-live.nvue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbbnVsbF0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBb0k7QUFDcEk7QUFDMkU7QUFDTDtBQUN0RTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDZ047QUFDaE4sZ0JBQWdCLGlOQUFVO0FBQzFCLEVBQUUsNkZBQU07QUFDUixFQUFFLGtHQUFNO0FBQ1IsRUFBRSwyR0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRSxzR0FBVTtBQUNaO0FBQ0E7O0FBRUE7QUFDQTtBQUNlLGdGIiwiZmlsZSI6IjQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucywgcmVjeWNsYWJsZVJlbmRlciwgY29tcG9uZW50cyB9IGZyb20gXCIuL2NyZWF0ZS1saXZlLm52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9M2RlN2E5MjgmbXBUeXBlPXBhZ2VcIlxudmFyIHJlbmRlcmpzXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL2NyZWF0ZS1saXZlLm52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmbXBUeXBlPXBhZ2VcIlxuZXhwb3J0ICogZnJvbSBcIi4vY3JlYXRlLWxpdmUubnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZtcFR5cGU9cGFnZVwiXG5mdW5jdGlvbiBpbmplY3RTdHlsZXMgKGNvbnRleHQpIHtcbiAgXG4gIGlmKCF0aGlzLm9wdGlvbnMuc3R5bGUpe1xuICAgICAgICAgIHRoaXMub3B0aW9ucy5zdHlsZSA9IHt9XG4gICAgICB9XG4gICAgICBpZihWdWUucHJvdG90eXBlLl9fbWVyZ2Vfc3R5bGUgJiYgVnVlLnByb3RvdHlwZS5fXyRhcHBTdHlsZV9fKXtcbiAgICAgICAgVnVlLnByb3RvdHlwZS5fX21lcmdlX3N0eWxlKFZ1ZS5wcm90b3R5cGUuX18kYXBwU3R5bGVfXywgdGhpcy5vcHRpb25zLnN0eWxlKVxuICAgICAgfVxuICAgICAgXG59XG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vQXBwbGljYXRpb25zL0hCdWlsZGVyWC5hcHAvQ29udGVudHMvSEJ1aWxkZXJYL3BsdWdpbnMvdW5pYXBwLWNsaS9ub2RlX21vZHVsZXMvQGRjbG91ZGlvL3Z1ZS1jbGktcGx1Z2luLXVuaS9wYWNrYWdlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBcImI0YjM1NmFlXCIsXG4gIGZhbHNlLFxuICBjb21wb25lbnRzLFxuICByZW5kZXJqc1xuKVxuXG5pbmplY3RTdHlsZXMuY2FsbChjb21wb25lbnQpXG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInBhZ2VzL2NyZWF0ZS1saXZlL2NyZWF0ZS1saXZlLm52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///4\n");

/***/ }),
/* 5 */
/*!******************************************************************************************************************************************!*\
  !*** /Users/caijizhou/Documents/HBuilderProjects/yoki-live/pages/create-live/create-live.nvue?vue&type=template&id=3de7a928&mpType=page ***!
  \******************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_template_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_7_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-hbuilderx/packages/webpack-uni-nvue-loader/lib/template.js!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--7-0!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./create-live.nvue?vue&type=template&id=3de7a928&mpType=page */ 6);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_template_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_7_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_template_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_7_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_template_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_7_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__["recyclableRender"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "components", function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_hbuilderx_packages_webpack_uni_nvue_loader_lib_template_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_7_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_template_id_3de7a928_mpType_page__WEBPACK_IMPORTED_MODULE_0__["components"]; });



/***/ }),
/* 6 */
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@dcloudio/vue-cli-plugin-hbuilderx/packages/webpack-uni-nvue-loader/lib/template.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--7-0!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!/Users/caijizhou/Documents/HBuilderProjects/yoki-live/pages/create-live/create-live.nvue?vue&type=template&id=3de7a928&mpType=page ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return recyclableRender; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "components", function() { return components; });
var components
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "scroll-view",
    {
      staticStyle: { flexDirection: "column" },
      attrs: {
        scrollY: true,
        showScrollbar: true,
        enableBackToTop: true,
        bubble: "true"
      }
    },
    [
      _c(
        "view",
        [
          _c("live-pusher", {
            ref: "livePusher",
            staticClass: ["livePusher"],
            style: "height:" + _vm.windowHeight + "px;",
            attrs: {
              id: "livePusher",
              url: _vm.url,
              mode: _vm.mode,
              muted: _vm.muted,
              enableCamera: _vm.enableCamera,
              autoFocus: _vm.autoFocus,
              devicePosition: _vm.position,
              beauty: _vm.beauty,
              whiteness: _vm.whiteness,
              aspect: _vm.aspect,
              orientation: _vm.orientation
            },
            on: {
              statechange: _vm.statechange,
              netstatus: _vm.netstatus,
              error: _vm.error
            }
          }),
          _c(
            "view",
            {
              staticStyle: {
                position: "fixed",
                left: "0",
                right: "0",
                height: "500rpx"
              },
              style: "top:" + _vm.statusBarHeight + "px;"
            },
            [
              _c(
                "view",
                {
                  staticClass: ["flex", "align-center", "justify-center"],
                  staticStyle: { width: "90rpx", height: "90rpx" }
                },
                [
                  _c("u-text", { staticClass: ["iconfont", "text-white"] }, [
                    _vm._v("")
                  ])
                ]
              ),
              _c(
                "view",
                {
                  staticClass: [
                    "position-absolute",
                    "rounded",
                    "p-2",
                    "flex",
                    "align-center"
                  ],
                  staticStyle: {
                    left: "90rpx",
                    right: "100rpx",
                    height: "160rpx",
                    backgroundColor: "rgba(0,0,0,0.2)"
                  }
                },
                [
                  _c(
                    "view",
                    {
                      staticClass: ["position-relative", "rounded"],
                      staticStyle: { height: "120rpx", width: "120rpx" }
                    },
                    [
                      _c("u-image", {
                        staticStyle: { height: "120rpx", width: "120rpx" },
                        attrs: { src: "/static/gift/3.png" }
                      }),
                      _c(
                        "u-text",
                        {
                          staticClass: [
                            "text-white",
                            "position-absolute",
                            "font"
                          ],
                          staticStyle: { left: "0", right: "0", bottom: "0" }
                        },
                        [_vm._v("更换封面")]
                      )
                    ],
                    1
                  ),
                  _c(
                    "view",
                    { staticClass: ["flex-1", "ml-2"] },
                    [
                      _c("u-input", {
                        staticClass: ["mb-2"],
                        attrs: {
                          type: "text",
                          value: "",
                          placeholder: "请输入直播标题"
                        }
                      }),
                      _c("u-text", { staticClass: ["text-white", "font"] }, [
                        _vm._v("#请选择分类")
                      ])
                    ],
                    1
                  )
                ]
              ),
              _c(
                "view",
                {
                  staticClass: [
                    "position-absolute",
                    "right-0",
                    "flex",
                    "flex-column"
                  ],
                  staticStyle: { width: "100rpx" }
                },
                [
                  _c(
                    "view",
                    {
                      staticClass: [
                        "flex",
                        "flex-column",
                        "align-center",
                        "justify-center"
                      ],
                      staticStyle: { height: "120rpx", width: "100rpx" }
                    },
                    [
                      _c(
                        "u-text",
                        { staticClass: ["iconfont", "text-white", "mb-1"] },
                        [_vm._v("")]
                      ),
                      _c("u-text", { staticClass: ["text-white", "font"] }, [
                        _vm._v("翻转")
                      ])
                    ]
                  ),
                  _c(
                    "view",
                    {
                      staticClass: [
                        "flex",
                        "flex-column",
                        "align-center",
                        "justify-center"
                      ],
                      staticStyle: { height: "120rpx", width: "100rpx" }
                    },
                    [
                      _c(
                        "u-text",
                        { staticClass: ["iconfont", "text-white", "mb-1"] },
                        [_vm._v("")]
                      ),
                      _c("u-text", { staticClass: ["text-white", "font"] }, [
                        _vm._v("画质")
                      ])
                    ]
                  ),
                  _c(
                    "view",
                    {
                      staticClass: [
                        "flex",
                        "flex-column",
                        "align-center",
                        "justify-center"
                      ],
                      staticStyle: { height: "120rpx", width: "100rpx" }
                    },
                    [
                      _c(
                        "u-text",
                        { staticClass: ["iconfont", "text-white", "mb-1"] },
                        [_vm._v("")]
                      ),
                      _c("u-text", { staticClass: ["text-white", "font"] }, [
                        _vm._v("美颜")
                      ])
                    ]
                  ),
                  _c(
                    "view",
                    {
                      staticClass: [
                        "flex",
                        "flex-column",
                        "align-center",
                        "justify-center"
                      ],
                      staticStyle: { height: "120rpx", width: "100rpx" }
                    },
                    [
                      _c(
                        "u-text",
                        { staticClass: ["iconfont", "text-white", "mb-1"] },
                        [_vm._v("")]
                      ),
                      _c("u-text", { staticClass: ["text-white", "font"] }, [
                        _vm._v("美白")
                      ])
                    ]
                  )
                ]
              )
            ]
          ),
          _c(
            "view",
            {
              staticClass: [
                "position-fixed",
                "bg-main",
                "flex",
                "align-center",
                "justify-center",
                "rounded-circle"
              ],
              staticStyle: {
                left: "100rpx",
                right: "100rpx",
                bottom: "100rpx",
                height: "120rpx"
              }
            },
            [
              _c("u-text", { staticClass: ["text-white", "font-md"] }, [
                _vm._v("开始视频直播")
              ])
            ]
          )
        ],
        1
      )
    ]
  )
}
var recyclableRender = false
var staticRenderFns = []
render._withStripped = true



/***/ }),
/* 7 */
/*!************************************************************************************************************************************!*\
  !*** /Users/caijizhou/Documents/HBuilderProjects/yoki-live/pages/create-live/create-live.nvue?vue&type=script&lang=js&mpType=page ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_ref_4_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_4_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/babel-loader/lib??ref--4-0!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--4-1!../../../../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./create-live.nvue?vue&type=script&lang=js&mpType=page */ 8);\n/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_ref_4_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_4_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_ref_4_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_4_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_ref_4_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_4_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_ref_4_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_4_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));\n /* harmony default export */ __webpack_exports__[\"default\"] = (_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_ref_4_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_4_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_create_live_nvue_vue_type_script_lang_js_mpType_page__WEBPACK_IMPORTED_MODULE_0___default.a); //# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbbnVsbF0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTZqQixDQUFnQix1a0JBQUcsRUFBQyIsImZpbGUiOiI3LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9BcHBsaWNhdGlvbnMvSEJ1aWxkZXJYLmFwcC9Db250ZW50cy9IQnVpbGRlclgvcGx1Z2lucy91bmlhcHAtY2xpL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTQtMCEuLi8uLi8uLi8uLi8uLi8uLi8uLi9BcHBsaWNhdGlvbnMvSEJ1aWxkZXJYLmFwcC9Db250ZW50cy9IQnVpbGRlclgvcGx1Z2lucy91bmlhcHAtY2xpL25vZGVfbW9kdWxlcy9AZGNsb3VkaW8vdnVlLWNsaS1wbHVnaW4tdW5pL3BhY2thZ2VzL3dlYnBhY2stcHJlcHJvY2Vzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNC0xIS4uLy4uLy4uLy4uLy4uLy4uLy4uL0FwcGxpY2F0aW9ucy9IQnVpbGRlclguYXBwL0NvbnRlbnRzL0hCdWlsZGVyWC9wbHVnaW5zL3VuaWFwcC1jbGkvbm9kZV9tb2R1bGVzL0BkY2xvdWRpby92dWUtY2xpLXBsdWdpbi11bmkvcGFja2FnZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL2NyZWF0ZS1saXZlLm52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmbXBUeXBlPXBhZ2VcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9BcHBsaWNhdGlvbnMvSEJ1aWxkZXJYLmFwcC9Db250ZW50cy9IQnVpbGRlclgvcGx1Z2lucy91bmlhcHAtY2xpL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTQtMCEuLi8uLi8uLi8uLi8uLi8uLi8uLi9BcHBsaWNhdGlvbnMvSEJ1aWxkZXJYLmFwcC9Db250ZW50cy9IQnVpbGRlclgvcGx1Z2lucy91bmlhcHAtY2xpL25vZGVfbW9kdWxlcy9AZGNsb3VkaW8vdnVlLWNsaS1wbHVnaW4tdW5pL3BhY2thZ2VzL3dlYnBhY2stcHJlcHJvY2Vzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tNC0xIS4uLy4uLy4uLy4uLy4uLy4uLy4uL0FwcGxpY2F0aW9ucy9IQnVpbGRlclguYXBwL0NvbnRlbnRzL0hCdWlsZGVyWC9wbHVnaW5zL3VuaWFwcC1jbGkvbm9kZV9tb2R1bGVzL0BkY2xvdWRpby92dWUtY2xpLXBsdWdpbi11bmkvcGFja2FnZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL2NyZWF0ZS1saXZlLm52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmbXBUeXBlPXBhZ2VcIiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///7\n");

/***/ }),
/* 8 */
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--4-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!/Users/caijizhou/Documents/HBuilderProjects/yoki-live/pages/create-live/create-live.nvue?vue&type=script&lang=js&mpType=page ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("/* WEBPACK VAR INJECTION */(function(__f__) {Object.defineProperty(exports, \"__esModule\", { value: true });exports.default = void 0; //\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\nvar _default =\n{\n  data: function data() {\n    return {\n      url: '', //推流地址，支持RTMP协议。\n      mode: 'SD', //推流视频模式，可取值：SD（标清）, HD（高清）, FHD（超清）。\n      muted: false, //是否静音。\n      enableCamera: true, //开启摄像头。\n      autoFocus: true, //\t自动聚集\n      position: 'front', //前置或后置，值为front, back\n      beauty: 9, //美颜，取值范围 0-9 ，0 表示关闭\n      whiteness: 9, //美白，取值范围 0-9 ，0 表示关闭\n      aspect: '6:19', //视频宽高比例\n      orientation: 'vertical', //画面方向\n      windowHeight: null,\n      context: null,\n      statusBarHeight: 0 };\n\n  },\n  onLoad: function onLoad() {\n    //获取推流控件高度\n    var res = uni.getSystemInfoSync();\n    this.windowHeight = res.windowHeight;\n    this.statusBarHeight = res.statusBarHeight;\n  },\n  onReady: function onReady() {\n    this.context = uni.createLivePusherContext('livePusher', this);\n    this.startPreview();\n  },\n  methods: {\n    //开启预览\n    startPreview: function startPreview() {\n      this.context.startPreview({\n        success: function success(e) {\n          __f__(\"log\", e, \" at pages/create-live/create-live.nvue:102\");\n        } });\n\n    },\n    //状态变化事件\n    statechange: function statechange() {\n\n    },\n    // 网络状态通知\n    netstatus: function netstatus() {},\n    // 渲染错误事件\n    error: function error() {} } };exports.default = _default;\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/vue-cli-plugin-uni/lib/format-log.js */ 9)[\"default\"]))//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVuaS1hcHA6Ly8vcGFnZXMvY3JlYXRlLWxpdmUvY3JlYXRlLWxpdmUubnZ1ZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9FQTtBQUNBLE1BREEsa0JBQ0E7QUFDQTtBQUNBLGFBREEsRUFDQTtBQUNBLGdCQUZBLEVBRUE7QUFDQSxrQkFIQSxFQUdBO0FBQ0Esd0JBSkEsRUFJQTtBQUNBLHFCQUxBLEVBS0E7QUFDQSx1QkFOQSxFQU1BO0FBQ0EsZUFQQSxFQU9BO0FBQ0Esa0JBUkEsRUFRQTtBQUNBLG9CQVRBLEVBU0E7QUFDQSw2QkFWQSxFQVVBO0FBQ0Esd0JBWEE7QUFZQSxtQkFaQTtBQWFBLHdCQWJBOztBQWVBLEdBakJBO0FBa0JBLFFBbEJBLG9CQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0F2QkE7QUF3QkEsU0F4QkEscUJBd0JBO0FBQ0E7QUFDQTtBQUNBLEdBM0JBO0FBNEJBO0FBQ0E7QUFDQSxnQkFGQSwwQkFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBSEE7O0FBS0EsS0FSQTtBQVNBO0FBQ0EsZUFWQSx5QkFVQTs7QUFFQSxLQVpBO0FBYUE7QUFDQSxhQWRBLHVCQWNBLEVBZEE7QUFlQTtBQUNBLFNBaEJBLG1CQWdCQSxFQWhCQSxFQTVCQSxFIiwiZmlsZSI6IjguanMiLCJzb3VyY2VzQ29udGVudCI6WyI8dGVtcGxhdGU+XG5cdDx2aWV3PlxuXHRcdDxsaXZlLXB1c2hlclxuXHRcdFx0aWQ9XCJsaXZlUHVzaGVyXCJcblx0XHRcdHJlZj1cImxpdmVQdXNoZXJcIlxuXHRcdFx0Y2xhc3M9XCJsaXZlUHVzaGVyXCJcblx0XHRcdDp1cmw9XCJ1cmxcIlxuXHRcdFx0Om1vZGU9XCJtb2RlXCJcblx0XHRcdDptdXRlZD1cIm11dGVkXCJcblx0XHRcdDplbmFibGUtY2FtZXJhPVwiZW5hYmxlQ2FtZXJhXCJcblx0XHRcdDphdXRvLWZvY3VzPVwiYXV0b0ZvY3VzXCJcblx0XHRcdDpkZXZpY2UtcG9zaXRpb249XCJwb3NpdGlvblwiXG5cdFx0XHQ6YmVhdXR5PVwiYmVhdXR5XCJcblx0XHRcdDp3aGl0ZW5lc3M9XCJ3aGl0ZW5lc3NcIlxuXHRcdFx0OmFzcGVjdD1cImFzcGVjdFwiXG5cdFx0XHQ6b3JpZW50YXRpb249XCJvcmllbnRhdGlvblwiXG5cdFx0XHRAc3RhdGVjaGFuZ2U9XCJzdGF0ZWNoYW5nZVwiXG5cdFx0XHRAbmV0c3RhdHVzPVwibmV0c3RhdHVzXCJcblx0XHRcdEBlcnJvcj1cImVycm9yXCJcblx0XHRcdDpzdHlsZT1cIidoZWlnaHQ6JyArIHdpbmRvd0hlaWdodCArICdweDsnXCJcblx0XHQ+PC9saXZlLXB1c2hlcj5cblx0XHRcblx0XHQ8dmlldyBzdHlsZT1cInBvc2l0aW9uOiBmaXhlZDtsZWZ0OiAwO3JpZ2h0OiAwO2hlaWdodDogNTAwcnB4O1wiIDpzdHlsZT1cIid0b3A6JytzdGF0dXNCYXJIZWlnaHQrJ3B4OydcIj5cblx0XHRcdDx2aWV3IGNsYXNzPVwiZmxleCBhbGlnbi1jZW50ZXIganVzdGlmeS1jZW50ZXJcIiBzdHlsZT1cIndpZHRoOiA5MHJweDtoZWlnaHQ6IDkwcnB4O1wiPlxuXHRcdFx0XHQ8dGV4dCBjbGFzcz1cImljb25mb250IHRleHQtd2hpdGVcIj4mI3hlNjA3OzwvdGV4dD5cblx0XHRcdDwvdmlldz5cblx0XHRcdFxuXHRcdFx0PHZpZXcgY2xhc3M9XCJwb3NpdGlvbi1hYnNvbHV0ZSByb3VuZGVkIHAtMiBmbGV4IGFsaWduLWNlbnRlclwiIHN0eWxlPVwibGVmdDogOTBycHg7cmlnaHQ6IDEwMHJweDtoZWlnaHQ6IDE2MHJweDtiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuMik7XCI+XG5cdFx0XHRcdDx2aWV3IHN0eWxlPVwiaGVpZ2h0OiAxMjBycHg7d2lkdGg6IDEyMHJweDtcIiBjbGFzcz1cInBvc2l0aW9uLXJlbGF0aXZlIHJvdW5kZWRcIj5cblx0XHRcdFx0XHQ8aW1hZ2Ugc3JjPVwiL3N0YXRpYy9naWZ0LzMucG5nXCIgc3R5bGU9XCJoZWlnaHQ6IDEyMHJweDt3aWR0aDogMTIwcnB4O1wiPjwvaW1hZ2U+XG5cdFx0XHRcdFx0PHRleHQgY2xhc3M9XCJ0ZXh0LXdoaXRlIHBvc2l0aW9uLWFic29sdXRlIGZvbnRcIiBzdHlsZT1cImxlZnQ6IDA7cmlnaHQ6IDA7Ym90dG9tOiAwO1wiPuabtOaNouWwgemdojwvdGV4dD5cblx0XHRcdFx0PC92aWV3PlxuXHRcdFx0XHQ8dmlldyBjbGFzcz1cImZsZXgtMSBtbC0yXCI+XG5cdFx0XHRcdFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgdmFsdWU9XCJcIiBwbGFjZWhvbGRlcj1cIuivt+i+k+WFpeebtOaSreagh+mimFwiIGNsYXNzPVwibWItMlwiLz5cblx0XHRcdFx0XHQ8dGV4dCBjbGFzcz1cInRleHQtd2hpdGUgZm9udFwiPiPor7fpgInmi6nliIbnsbs8L3RleHQ+XG5cdFx0XHRcdDwvdmlldz5cblx0XHRcdDwvdmlldz5cblx0XHRcdFxuXHRcdFx0PHZpZXcgY2xhc3M9XCJwb3NpdGlvbi1hYnNvbHV0ZSByaWdodC0wIGZsZXggZmxleC1jb2x1bW4gXCIgc3R5bGU9XCJ3aWR0aDogMTAwcnB4O1wiPlxuXHRcdFx0XHQ8dmlldyBzdHlsZT1cImhlaWdodDogMTIwcnB4O3dpZHRoOiAxMDBycHg7XCIgY2xhc3M9XCJmbGV4IGZsZXgtY29sdW1uIGFsaWduLWNlbnRlciBqdXN0aWZ5LWNlbnRlclwiPlxuXHRcdFx0XHRcdDx0ZXh0IGNsYXNzPVwiaWNvbmZvbnQgdGV4dC13aGl0ZSBtYi0xXCI+JiN4ZTYwNTs8L3RleHQ+XG5cdFx0XHRcdFx0PHRleHQgY2xhc3M9XCJ0ZXh0LXdoaXRlIGZvbnRcIj7nv7vovaw8L3RleHQ+XG5cdFx0XHRcdDwvdmlldz5cblx0XHRcdFx0PHZpZXcgc3R5bGU9XCJoZWlnaHQ6IDEyMHJweDt3aWR0aDogMTAwcnB4O1wiIGNsYXNzPVwiZmxleCBmbGV4LWNvbHVtbiBhbGlnbi1jZW50ZXIganVzdGlmeS1jZW50ZXJcIj5cblx0XHRcdFx0XHQ8dGV4dCBjbGFzcz1cImljb25mb250IHRleHQtd2hpdGUgbWItMVwiPiYjeGU2MGM7PC90ZXh0PlxuXHRcdFx0XHRcdDx0ZXh0IGNsYXNzPVwidGV4dC13aGl0ZSBmb250XCI+55S76LSoPC90ZXh0PlxuXHRcdFx0XHQ8L3ZpZXc+XG5cdFx0XHRcdDx2aWV3IHN0eWxlPVwiaGVpZ2h0OiAxMjBycHg7d2lkdGg6IDEwMHJweDtcIiBjbGFzcz1cImZsZXggZmxleC1jb2x1bW4gYWxpZ24tY2VudGVyIGp1c3RpZnktY2VudGVyXCI+XG5cdFx0XHRcdFx0PHRleHQgY2xhc3M9XCJpY29uZm9udCB0ZXh0LXdoaXRlIG1iLTFcIj4mI3hlNjMyOzwvdGV4dD5cblx0XHRcdFx0XHQ8dGV4dCBjbGFzcz1cInRleHQtd2hpdGUgZm9udFwiPue+juminDwvdGV4dD5cblx0XHRcdFx0PC92aWV3PlxuXHRcdFx0XHQ8dmlldyBzdHlsZT1cImhlaWdodDogMTIwcnB4O3dpZHRoOiAxMDBycHg7XCIgY2xhc3M9XCJmbGV4IGZsZXgtY29sdW1uIGFsaWduLWNlbnRlciBqdXN0aWZ5LWNlbnRlclwiPlxuXHRcdFx0XHRcdDx0ZXh0IGNsYXNzPVwiaWNvbmZvbnQgdGV4dC13aGl0ZSBtYi0xXCI+JiN4ZTYzMTs8L3RleHQ+XG5cdFx0XHRcdFx0PHRleHQgY2xhc3M9XCJ0ZXh0LXdoaXRlIGZvbnRcIj7nvo7nmb08L3RleHQ+XG5cdFx0XHRcdDwvdmlldz5cblx0XHRcdDwvdmlldz5cblx0XHRcdFxuXHRcdDwvdmlldz5cblx0XHRcblx0XHRcblx0XHQ8dmlldyBjbGFzcz1cInBvc2l0aW9uLWZpeGVkIGJnLW1haW4gZmxleCBhbGlnbi1jZW50ZXIganVzdGlmeS1jZW50ZXIgcm91bmRlZC1jaXJjbGVcIiBzdHlsZT1cImxlZnQ6IDEwMHJweDtyaWdodDogMTAwcnB4O2JvdHRvbTogMTAwcnB4O2hlaWdodDogMTIwcnB4O1wiPlxuXHRcdFx0PHRleHQgY2xhc3M9XCJ0ZXh0LXdoaXRlIGZvbnQtbWRcIj7lvIDlp4vop4bpopHnm7Tmkq08L3RleHQ+XG5cdFx0PC92aWV3PlxuXHRcdFxuXHQ8L3ZpZXc+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuXHRkYXRhKCkge1xuXHRcdHJldHVybiB7XG5cdFx0XHR1cmw6ICcnLCAvL+aOqOa1geWcsOWdgO+8jOaUr+aMgVJUTVDljY/orq7jgIJcblx0XHRcdG1vZGU6ICdTRCcsIC8v5o6o5rWB6KeG6aKR5qih5byP77yM5Y+v5Y+W5YC877yaU0TvvIjmoIfmuIXvvIksIEhE77yI6auY5riF77yJLCBGSETvvIjotoXmuIXvvInjgIJcblx0XHRcdG11dGVkOiBmYWxzZSwgLy/mmK/lkKbpnZnpn7PjgIJcblx0XHRcdGVuYWJsZUNhbWVyYTogdHJ1ZSwgLy/lvIDlkK/mkYTlg4/lpLTjgIJcblx0XHRcdGF1dG9Gb2N1czogdHJ1ZSwgLy9cdOiHquWKqOiBmumbhlxuXHRcdFx0cG9zaXRpb246ICdmcm9udCcsIC8v5YmN572u5oiW5ZCO572u77yM5YC85Li6ZnJvbnQsIGJhY2tcblx0XHRcdGJlYXV0eTogOSwgLy/nvo7popzvvIzlj5blgLzojIPlm7QgMC05IO+8jDAg6KGo56S65YWz6ZetXG5cdFx0XHR3aGl0ZW5lc3M6IDksIC8v576O55m977yM5Y+W5YC86IyD5Zu0IDAtOSDvvIwwIOihqOekuuWFs+mXrVxuXHRcdFx0YXNwZWN0OiAnNjoxOScsIC8v6KeG6aKR5a696auY5q+U5L6LXG5cdFx0XHRvcmllbnRhdGlvbjogJ3ZlcnRpY2FsJywgLy/nlLvpnaLmlrnlkJFcblx0XHRcdHdpbmRvd0hlaWdodDogbnVsbCxcblx0XHRcdGNvbnRleHQ6bnVsbCxcblx0XHRcdHN0YXR1c0JhckhlaWdodDowXG5cdFx0fTtcblx0fSxcblx0b25Mb2FkKCkge1xuXHRcdC8v6I635Y+W5o6o5rWB5o6n5Lu26auY5bqmXG5cdFx0bGV0IHJlcz11bmkuZ2V0U3lzdGVtSW5mb1N5bmMoKVxuXHRcdHRoaXMud2luZG93SGVpZ2h0PXJlcy53aW5kb3dIZWlnaHRcblx0XHR0aGlzLnN0YXR1c0JhckhlaWdodCA9IHJlcy5zdGF0dXNCYXJIZWlnaHRcblx0fSxcblx0b25SZWFkeSgpIHtcblx0XHR0aGlzLmNvbnRleHQ9IHVuaS5jcmVhdGVMaXZlUHVzaGVyQ29udGV4dCgnbGl2ZVB1c2hlcicsIHRoaXMpXG5cdFx0dGhpcy5zdGFydFByZXZpZXcoKVxuXHR9LFxuXHRtZXRob2RzOiB7XG5cdFx0Ly/lvIDlkK/pooTop4hcblx0XHRzdGFydFByZXZpZXcoKXtcblx0XHRcdHRoaXMuY29udGV4dC5zdGFydFByZXZpZXcoe1xuXHRcdFx0XHRzdWNjZXNzOihlKT0+e1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGUpXG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0fSxcblx0XHQvL+eKtuaAgeWPmOWMluS6i+S7tlxuXHRcdHN0YXRlY2hhbmdlKCkge1xuXHRcdFx0XG5cdFx0fSxcblx0XHQvLyDnvZHnu5znirbmgIHpgJrnn6Vcblx0XHRuZXRzdGF0dXMoKSB7fSxcblx0XHQvLyDmuLLmn5PplJnor6/kuovku7Zcblx0XHRlcnJvcigpIHt9XG5cdH1cbn07XG48L3NjcmlwdD5cblxuPHN0eWxlPjwvc3R5bGU+XG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///8\n");

/***/ }),
/* 9 */
/*!*********************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/lib/format-log.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.log = log;exports.default = formatLog;function typof(v) {
  var s = Object.prototype.toString.call(v);
  return s.substring(8, s.length - 1);
}

function isDebugMode() {
  /* eslint-disable no-undef */
  return typeof __channelId__ === 'string' && __channelId__;
}

function jsonStringifyReplacer(k, p) {
  switch (typof(p)) {
    case 'Function':
      return 'function() { [native code] }';
    default:
      return p;}

}

function log(type) {
  for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }
  console[type].apply(console, args);
}

function formatLog() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }
  var type = args.shift();
  if (isDebugMode()) {
    args.push(args.pop().replace('at ', 'uni-app:///'));
    return console[type].apply(console, args);
  }

  var msgs = args.map(function (v) {
    var type = Object.prototype.toString.call(v).toLowerCase();

    if (type === '[object object]' || type === '[object array]') {
      try {
        v = '---BEGIN:JSON---' + JSON.stringify(v, jsonStringifyReplacer) + '---END:JSON---';
      } catch (e) {
        v = type;
      }
    } else {
      if (v === null) {
        v = '---NULL---';
      } else if (v === undefined) {
        v = '---UNDEFINED---';
      } else {
        var vType = typof(v).toUpperCase();

        if (vType === 'NUMBER' || vType === 'BOOLEAN') {
          v = '---BEGIN:' + vType + '---' + v + '---END:' + vType + '---';
        } else {
          v = String(v);
        }
      }
    }

    return v;
  });
  var msg = '';

  if (msgs.length > 1) {
    var lastMsg = msgs.pop();
    msg = msgs.join('---COMMA---');

    if (lastMsg.indexOf(' at ') === 0) {
      msg += lastMsg;
    } else {
      msg += '---COMMA---' + lastMsg;
    }
  } else {
    msg = msgs[0];
  }

  console[type](msg);
}

/***/ }),
/* 10 */
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })
/******/ ]);